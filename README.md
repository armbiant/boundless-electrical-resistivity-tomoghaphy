# Boundless Electrical Resistivity Tomography (BERT)

Build status: [![Build Status](http://jenkins.pygimli.org/job/pyBERT/badge/icon)](http://jenkins.pygimli.org/job/pyBERT/)

BERT is a software package for modelling & inversion of ERT data.
It has been traditionally used as C++ apps based on the pyGIMLi core plus bash 
scripts for command line, but increasingly uses Python through
pyGIMLi and pyBERT, not only for visualization but also for meshing and computing.

For simple ERT inversion, you don't need BERT anymore but the `ERTManager` in the
ERT module of pyGIMLi (see [pygimli.org](http://www.pygimli.org) examples). You
might need BERT for
* multi-frequency FD induced polarization with the `FDIP` class
* full-decay TD induced polarization with the `TDIP` class
* importing and exporting a lot of different file formats 
* special ERT-specific data processing, e.g. reciprocal analysis 
* inversions using the command line 

You may take a look at our 
[Tutorial](http://www.resistivity.net/download/bert-tutorial.pdf) first to learn
the way of doing inversions BERT v2. BERT v2.2 was based upon pyGIMLi 1.0.x, while
BERT v2.3 is based on pyGIMLi 1.1/1.2. 
In the future, BERT v3 will avoid binary executables and thus be entirely in Python.

See also the pyGIMLi webpage
[www.pygimli.org](http://www.pygimli.org)

## Installation with conda [![Anaconda-Server Badge](https://anaconda.org/gimli/pybert/badges/installer/conda.svg)](https://conda.anaconda.org/gimli) [![Anaconda-Server Badge](https://anaconda.org/gimli/pybert/badges/downloads.svg)](https://anaconda.org/gimli/pybert)[![Anaconda-Server Badge](https://anaconda.org/gimli/pybert/badges/version.svg)](https://anaconda.org/gimli/pybert)[![Anaconda-Server Badge](https://anaconda.org/gimli/pybert/badges/latest_release_date.svg)](https://anaconda.org/gimli/pybert)

The most comfortable way to install BERT is
via the conda package manager contained in the [Anaconda
distribution](https://docs.continuum.io/anaconda/install#linux-install).
Anaconda is a scientific Python distribution with hundreds of useful packages
included (~400 Mb). You can also use the lightweight alternative
[Miniconda](http://conda.pydata.org/miniconda.html) (~35 Mb) and only install
the packages you need or like. Notes on how to install Miniconda (without root
privileges) can be found
[here](https://gitlab.com/resistivity-net/bert/wikis/install-miniconda).
pyGIMLi packages are available for Python 3.7, 3.8 and 3.9. We eventually
dropped support for Python 3.5 and 3.6 and will support 3.10 soon.

```bash
# Install pybert (and all dependencies such as pygimli, numpy, mpl, tetgen)
conda install -c gimli -c conda-forge pybert

# After installation, you can try out the examples for testing
cd ~/miniconda3/share/examples/inversion/2dflat/gallery
bert gallery.cfg all
bert gallery.cfg show

# Update to a newer version
conda update -f pygimli pybert
```

Note that the conda-forge channel is used for installing tetgen.
Consider creating a new environment where all is installed by one line
```bash
conda create -n bert -c gimli -c conda-forge pybert
conda activate bert
```

If you only working in Python, it is easier to checkout BERT by git
and add the `bert/python` path to the `$PYTHONPATH$`.

## Windows binary installers

Download the latest binary installer for 64bit Windows (7, 8, 10, 11):

[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.9%20bert2.4.1-green.svg)](http://www.resistivity.net/download/setup-bert2.4.1win64py39.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.8%20bert2.4.0-green.svg)](http://www.resistivity.net/download/setup-bert2.4.0win64py38.exe)

[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.8%20bert2.3.4-green.svg)](http://www.resistivity.net/download/setup-bert2.3.4win64py38.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.8%20bert2.3.3-green.svg)](http://www.resistivity.net/download/setup-bert2.3.3win64py38.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.8%20bert2.3.2-green.svg)](http://www.resistivity.net/download/setup-bert2.3.2win64py38.exe)

BERT v2.3 is based on pyGIMLi v1.1-1.2, which brings along some markable changes over v1.0.
In the binary installers, pyGIMLi is included.

The current version works with any Python 3.8, we recommend the [Anaconda Distribution](https://www.anaconda.com/download/).
There are older installers (for Python 3.6 and 3.7) below bringing along pyGIMLi 1.0.x versions.

Please read the [Tutorial](http://www.resistivity.net/download/bert-tutorial.pdf), particularly Appendix A for Windows users:
The path to BERT and to Python must be known to the system by either the System Control (Extended Options) or in the command line of the MSYS shell (download the x86_64 variant from https://msys2.github.io) that is needed to run BERT.
Assume BERT is installed in `C:\Software\BERT` and Anaconda in `C:\Software\Anaconda`
```bash
export PATH=$PATH:/c/Software/BERT:/c/Software/Anaconda
bert version  # should yield the BERT version
```
Additionally, the BERT directory must be in the search Path for Python modules to find pyGIMLi:
```bash
export PYTHONPATH=$PYTHONPATH:/c/Software/BERT
python -c "import pygimli;print(pygimli.__version__)" # should yield the pyGIMLi version
```

To do this for every start, append the lines to your startup file:
```bash
echo 'export PATH=$PATH:/c/Software/BERT:/c/Software/Anaconda' >> $HOME/.bashrc
echo 'export PYTHONPATH=$PYTHONPATH:/c/Software/BERT' >> $HOME/.bashrc
```
If you are unable to view outputs from 'show' commands, you may also need to add the following to your '.bashrc'
```
export PATH=$PATH:$(dirname `which python`)/Scripts
export PATH=$PATH:$(dirname `which python`)/Library/bin
```

### Older versions (also for other Python versions)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.8%20bert2.3.1-green.svg)](http://www.resistivity.net/download/setup-bert2.3.1win64py38.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.7%20bert2.3.1-green.svg)](http://www.resistivity.net/download/setup-bert2.3.1win64py37.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.7%20bert2.3.0-green.svg)](http://www.resistivity.net/download/setup-bert2.3.0win64py37.exe)

[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.7%20bert2.2.12-green.svg)](http://www.resistivity.net/download/setup-bert2.2.12win64py37.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.7%20bert2.2.11-green.svg)](http://www.resistivity.net/download/setup-bert2.2.11win64py37.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.7%20bert2.2.10-green.svg)](http://www.resistivity.net/download/setup-bert2.2.10win64py37.exe)


## Update your installation

To update your installation go into your BERT source repository.

```bash
cd bert/bert
git pull
```

Then you need to rebuild BERT.

    cd ../build
    make
    make bert1

If something goes wrong try to clean your build directory and repeat from the
cmake command above.

# The unified data format

All projects in our working group use a unified format such that data are 
transpararent and transportable. There are a lot of import and export filters 
for third party software or hardware, but internally the following structure 
is used. Since the most applications use multi-electrode systems they are based 
on the way they are addressed (and computed in modeling).

Every data set consists of two mandatory parts: electrode definitions and 
arrangement definition. Subsequently the number of electrodes, 
their positions (x z for 2D, x y z for 3D), the number of data and their 
definitions are given. Comments may be places after the # character. 

After the number of data the definition for each datum is given row-wise 
by the electrode numbers for the current electrodes A and B (C1 and C2) and the 
potential electrodes M and N (P1 and P2). Please not all electrode number starting
from one. Electrodes with a zero number will be treated as infinity electrodes.
They may be followed by other attributes (see list below). 
Standard is the apparent resistivity and optional the relative error as 
exemplified for a tiny dipole-dipole survey.

```
6# Number of electrodes
# x z position for each electrode
0     0
1     0
2     0 # loose ground
3     0
4     0
5     0
6# Number of data
# a b m n rhoa
1   2   3   4  231.2
2   3   4   5  256.7
3   4   5   6  312.8
1   2   4   5  12.1 # possibly an outlier
2   3   5   6  199.7
1   2   5   6  246.2
```

If other fields or order are used, a token string in the line after the data 
number specifies the given fields. The token may be followed by a slash and
a physical unit. The following tokens are allowed (case insensitive).
Tokens	Meaning	possible units

* `a` `c1` electrode number for A (C1)
* `b` `c2` electrode number for B (C2)
* `m` `p1`	electrode number for M (P1)
* `n` `p2`	electrode number for N (P2)
* `rhoa` `Ra`	apparent resistivity	Ohmmeter
* `rho` `r`	Resistance	Ohm
* `err`	relative measurement error in %/100 (default)
* `ip` IP measure	mRad(default)
* `ipErr` absolute IP measure error in mRad(default)
* `i` `I` Current	A(default),mA,uA
* `u` `U` Voltage	V(default),mV,uV

The following sample contains voltages, currents and a percentage error.

```
...
6# Number of data
# a b m n u i/mA err/% 
  1   2   3   4  -0.5305165 102.2 2.4
  2   3   4   5  -0.5305165 99.9  1.4
  3   4   5   6  -0.5305165 95.6  2.6
  1   2   4   5  -0.1326291 100.1 7.6
  2   3   5   6  -0.1326291 80.2 8.6
  1   2   5   6  -0.05305165 77.3 7.5
```

Topography may be given in two ways.

1. Each electrode can be given a height value as z position (preferred by webinv).
2. Alternatively, the data may be followed by a topo list (preferred by DC2dInvRes).

```
...
  1   2   5   6  -0.05305165  7.5
4# Number of topo points
# x h for each topo point
0 353.2
12 357.1
19 359.9
24.5 350
```
