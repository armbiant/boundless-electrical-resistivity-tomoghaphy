Publications related to BERT inversion or modelling

############ Main development ###############
Papers:
R�cker,G�nther&Spitzer (2006): 3-D modeling and inversion of DC resistivity data incorporating topography - Part I: Modeling. - Geophys. J. Int., 166, 495-505, doi:10.1111/j.1365-246X.2006.03010.x.
G�nther,R�cker&Spitzer (2006): 3-D modeling and inversion of DC resistivity data incorporating topography - Part II: Inversion. - Geophys. J. Int. 166, 506-517, doi:10.1111/j.1365-246X.2006.03011.x.
R�cker&G�nther (2011): The simulation of finite ERT electrodes using the complete electrode model. Geophysics 76 (4), F227�F238, doi:10.1190/1.3581356.

Ext. Abstracts:
G�nther&R�cker(2006): A general approach for introducing information into inversion and examples from dc resistivity inversion. In Ext. Abstract, EAGE Near Surface Geophysics Workshop. 4.-6.9.06, Helsinki(Finland).
G�nther&R�cker(2008): The effect of finite electrodes on ERT measurements. EAGE Near Surface Geophysics meeting. Krakow, Poland.
G�nther,R�cker,Igel&Furche(2009): Advanced inversion strategies using a new Geophysical Inversion and Modelling Library. In Ext. Abstract, EAGE Near Surface Geophysics Workshop. 4.-6.9.06, Helsinki(Finland).

Poster:
G�nther&R�cker(2005): Applications of the Triple-grid Technique to the Inversion of DC Resistivity Data. Poster&Ext.Abstr. EMTF meeting.
G�nther&R�cker(2007): A general approach for introducing structural information � from a-priori data to a new joint inversion algorithm. Poster. AGU spring meeting.
simulated with the complete electrode model
G�nther&R�cker(2010): Advanced resistivity inversion using a new generation of BERT v. 2.0 - Examples. Poster - DGG.

Forthcoming:
R�cker&G�nther: Advanced ERT

############ Applications #############
Papers:
Doetsch et al.(2010): The borehole-fluid effect in electrical resistivity imaging. - Geophysics, 75(4), F107-F114.
Flechsig et al. (2010): Geoelectrical Investigations in the Cheb Basin/W-Bohemia: An Approach to Evaluate the Near-Surface conductivity structure: Stud. Geophys. Geod., 54, 417�437.
Garre et al.(2010): Comparison of Heterogeneous Transport processes observed with ERT in two different soils. - Vadoze Zone Journal 9(2), 336-349.
Heincke et al.(2010): Combined three-dimensional electric and seismic tomography study on the �knes rockslide in western Norway. Journal of Applied Geophysics 70(4): 292-306.
Updhuay et al.(2011): Three-dimensional resistivity tomography in extreme coastal terrain amidst dense cultural signals: application to cliff stability assessment at the historic D-Day site. - Geophysical Journal International 185(1), 201-220.
Coscia et al. (2011): 3D crosshole apparent resistivity static inversion and monitoring of a coupled river-aquifer system. - Geophysics 76(2), G49-59.
Doetsch et al. (2011): Constraining 3-D electrical resistance tomography with GPR reflection data for improved aquifer characterization, Journal of Applied Geophysics, 2012, 78, 68
Ext. Abstracts (not covered by full papers):

Poster (not covered by full papers):
Mohnke et al.(2008): 3D Electrical Resistivity Tomography (ERT) as a tool for monitoring small scale soil moisture dynamics to assess the impact of earthworm activity on water repellency. EGU.
Furche et al.(2008): 3D imaging of infiltrating water in a lysimeter using electrical resistivity tomography (ERT). EGU
Bechtold et al. (2010): Preferential upward flow in soils: A 3D comparison of modeled and ERT-derived data from a salt tracer experiment under evaporation conditions. EGU
