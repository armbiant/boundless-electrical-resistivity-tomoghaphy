HOWTO - Make simulations on a Hydrus3D mesh

Task: Use discretization and resistivity vector from Hydrus3D simulation
Problem: Append unstructured mesh boundary to (usually regular) tetrahedral mesh

With the monitoring of hydraulic processes using ERT the demand for an efficient
forward calculation for a given water content and thus resistivity arises.
Hydrus3D uses meshes of tetrahedra that are usually regularly arranged. For an
appropriate forward calculation boundaries far away from the parameters are needed.
A further regular prolongation will cause a huge increase of nodes and thus runtime.
Therefore the parameter box is surrounded by unstructed tetrahedra whose resistivity
is determined using parameter prolongation as done in inversion.
The problem is very similar to the one of creating regular 3d meshes for inversion,
except that (a) the input is already a tetrahedal mesh and (b) the output must be
defined such that a forward calculation using dcmod can directly called.
According to (a), the first part of creating the mesh and refining it is omitted.
Instead, the Hydrus mesh is imported and saved using the following pygimli script.
(Unzip MESHTRIA.TXT.gz using gzip -d to obtain the Hydrus3D mesh)

    import pygimli as g

    mesh=g.Mesh()
    f=open('MESHTRIA.TXT','r')
    for i in range(6):
        line1=f.readline()
        
    nnodes=int(line1.split()[0])
    ncells=int(line1.split()[1])
    print nnodes, ncells
    line1=f.readline()
    nodes=[]
    for ni in range(nnodes):
        pos=f.readline().split()
        p=g.RVector3(float(pos[1]),float(pos[2]),float(pos[3]))
        n=mesh.createNode(p)
        nodes.append(n)

    line1=f.readline()
    line1=f.readline()
    cells=[]
    for ci in range(ncells):
        pos=f.readline().split()
        i,j,k,l=int(pos[1]),int(pos[2]),int(pos[3]),int(pos[4]),
        c=mesh.createTetrahedron(nodes[i-1],nodes[j-1],nodes[k-1],nodes[l-1])
        cells.append(c)

    f.close()

    print mesh
    mesh.save('hydrus')
    mesh.exportVTK('hydrus')

As a result, we have hydrus.bms and hydrus.vtk and can visualize it using ParaView.
Just as in regular3d meshes, its boundary is exported as vtk file.
    mesh.createNeighbourInfos()
    for b in mesh.boundaries():
        if not ( b.leftCell() and b.rightCell() ):
            b.setMarker(1)

    poly = g.Mesh()
    poly.createMeshByBoundaries( mesh, mesh.findBoundaryByMarker( 1 ) );
    poly.exportAsTetgenPolyFile('paraBoundary')
    
The rest is just as in the other example, i.e.
- Part 2: create world, mesh to obtain boundary
- Part 3: merge surfaces of inner and outer box and mesh together
except that the markers for the individual cells are not constantly 2 (inversion),
but counted starting from 1.
    # merge inner mesh into outer
    for m,c in enumerate(Inner.cells()): 
        nodes = g.stdVectorNodes( )
        for i, n in enumerate( c.nodes() ):
            nodes.append( Outer.createNodeWithCheck( n.pos() ) )                
        Outer.createCell( nodes, m+1 );

Note further that the cells in the outer box keep the marker 0.
In order to make a forward calculation we first need to map the markers into 
resistivities by creating a two-column rho.map file starting with 0 0 (marker zero
is mapped into 0 resistivity, which means filling up with neighboring values):

    import numpy as N
    res=...     # load from file
    ind=N.arange(len(res)+1)
    A=N.vstack((ind,N.hstack((0,res)))).T
    f=open('rho.map','w')
    for row in A: 
        f.write('%d' % row[0] + '\t' + '%f' % row[1] + '\n')
    f.close()

The final call to dcmod then reads
    dcmod -v -S -a rho.map -s datafile mesh.bms