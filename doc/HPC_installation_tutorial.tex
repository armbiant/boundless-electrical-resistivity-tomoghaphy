\documentclass[a4, titlepage]{scrartcl}
\usepackage{color}
\usepackage{xcolor}
\usepackage{textcomp}
\usepackage{listings}
\usepackage{url}
\usepackage[pdftex,pdfpagelabels=true,plainpages=false]{hyperref}
% Farben
\usepackage{color}
\usepackage{colortbl}
\definecolor{darkblue}{rgb}{0,0,.5}
\hypersetup{
%		plainpages=false,				% prevents error from page i / page 1
    bookmarks=true,         % show bookmarks bar?
    unicode=false,          % non-Latin characters in Acrobats bookmarks
    pdftoolbar=true,        % show Acrobats toolbar?
    pdfmenubar=true,        % show Acrobats menu?
    pdffitwindow=true,      % page fit to window when opened    ´
    pdftitle={BERT on BRUTUS - Installation Guide},    % title
    pdfauthor={Florian Wagner},     % author
    pdfnewwindow=true,      % links in new window
    colorlinks=true,       % false: boxed links; true: colored links
    linkcolor=black,    % color of internal links
    citecolor=darkblue,        % color of links to bibliography
    filecolor=blue,      % color of file links
    urlcolor=darkblue,           % color of external links
    bookmarksnumbered=true, %Lesezeichen werden nummeriert dargestellt
		pdfdisplaydoctitle=true,
		pdfstartview=FitV,
}

% Std
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{textcomp}
\usepackage{multirow}
\usepackage[ngerman,english]{babel}

% Solarized for code listings 
\definecolor{solarized@base03}{HTML}{002B36}
\definecolor{solarized@base02}{HTML}{073642}
\definecolor{solarized@base01}{HTML}{586e75}
\definecolor{solarized@base00}{HTML}{657b83}
\definecolor{solarized@base0}{HTML}{839496}
\definecolor{solarized@base1}{HTML}{93a1a1}
\definecolor{solarized@base2}{HTML}{EEE8D5}
\definecolor{solarized@base3}{HTML}{FDF6E3}
\definecolor{solarized@yellow}{HTML}{B58900}
\definecolor{solarized@orange}{HTML}{CB4B16}
\definecolor{solarized@red}{HTML}{DC322F}
\definecolor{solarized@magenta}{HTML}{D33682}
\definecolor{solarized@violet}{HTML}{6C71C4}
\definecolor{solarized@blue}{HTML}{268BD2}
\definecolor{solarized@cyan}{HTML}{2AA198}
\definecolor{solarized@green}{HTML}{859900}
 
\lstset{
  language=bash,
  upquote=true,
  columns=fixed,
  tabsize=4,
  extendedchars=true,
  breaklines=true,
  frame=shadowbox,
  numbers=left,
  numbersep=10pt,
  morekeywords={*, username@brutus1, make, bsub, module, svn, ssh, sh,*},
  deletekeywords={*, for, complete, .svn, .sh, *},
  showstringspaces=false,
  rulesepcolor=\color{solarized@base03},
  numberstyle=\tiny\color{solarized@base01},
  basicstyle=\footnotesize\ttfamily\color{solarized@base00},
  keywordstyle=\color{solarized@green},
  stringstyle=\color{solarized@cyan}\ttfamily,
  identifierstyle=\color{solarized@blue},
  commentstyle=\color{solarized@base01},
  emphstyle=\color{solarized@red},
  backgroundcolor=\color{solarized@base3},
  belowskip=2em,
  aboveskip=1.5em
}

%Kopf- und Fußzeile
\usepackage{fancyhdr}
%\renewcommand{\chaptermark}[1]{\markboth{\thechapter.\space#1}{}}
\cfoot{}

%Kopfzeile links bzw. innen
\fancyhead[L]{BERT on BRUTUS Installation Guide}
%Kopfzeile rechts bzw. außen
\fancyhead[R]{\slshape \nouppercase \leftmark}
%Linie oben
\renewcommand{\headrulewidth}{0.3pt}

%Fußzeile links bzw. innen
\fancyfoot[L]{\href{mailto:fwagner@gfz-potsdam.de?subject=BERT on BRUTUS}{fwagner@gfz-potsdam.de}}
%Fußzeile rechts bzw. außen
\fancyfoot[R]{\thepage}
%Linie unten
\renewcommand{\footrulewidth}{0.3pt}

%Zeilenabstand
\renewcommand{\baselinestretch}{1.10}\normalsize

% Title Page
\title{BERT on BRUTUS \\ \vspace{1.5 cm} \Large{Installation guide to setup the BERT/GIMLi software environment on ETH's High-Performance-Computing Cluster}\vspace{5 cm}}
\author{Florian Wagner (\href{mailto:fwagner@gfz-potsdam.de?subject=BERT on BRUTUS}{fwagner@gfz-potsdam.de})}

\begin{document}
\maketitle
\pagestyle{fancy}
\section{General Information}

This is a step-by-step installation guide to setup the BERT (Boundless Electrical Resistivity Tomography) and GIMLi (Geophysical Inversion and Modeling Library) software packages on ETH's High-Performance-Computing Cluster named BRUTUS. This document is intended to support staff and students of the Institute of Geophysics and focuses on BRUTUS-specific aspects. It is neither the objective to provide a general-purpose installation guide nor a comprehensive documentation. For further information, the reader is referred to the official manual of the software packages and the BRUTUS web pages. A brief overview and additional sources of information are given below.
\vfill
\minisec{Boundless Electrical Resistivity Tomography}\vfill
BERT is a highly versatile tool for electrical resistivity imaging, which is freely available for academic purposes. Due to the use of unstructured finite-element meshes, it provides all the necessary flexibility to deal with resistivity data that is considerably influenced by topography. Methodological aspects are discussed in detail in the following papers:
\\
\selectlanguage{ngerman}
\\
C. Rücker, T. Günther and K. Spitzer (2006): \textit{3-D modeling and inversion of DC resistivity data incorporating topography - Part I: Modeling.} - Geophys. J. Int., 166, 495-505, \href{http://onlinelibrary.wiley.com/doi/10.1111/j.1365-246X.2006.03010.x/full}{doi: 10.1111/j.1365-246X.2006.03010.x}.
\\
\\
T. Günther, C. Rücker and K. Spitzer (2006): \textit{3-D modeling and inversion of DC resistivity data incorporating topography - Part II: Inversion.} - Geophys. J. Int. 166, 506-517, \href{http://onlinelibrary.wiley.com/doi/10.1111/j.1365-246X.2006.03010.x/full}{doi: 10.1111/j.1365-246X.2006.03011.x}.
\\
\selectlanguage{english}
\\
Further information can be found on \url{www.resistivity.net} and in the official BERT tutorial. BERT makes use (and therefore depends) on GIMLi.
\vfill
\minisec{Geophysical Inversion and Modeling Library}
\vfill
GIMLi is a multi-method library for solving geophysical forward and inverse problems written in C++ and Python. The consequent use of template programming allows for tailoring individual inversion problems with various minimization and regularization schemes, different transformation functions and the ability to incorporate joint parametric and structural inversion.
\\

GIMLi is released under the GNU General Public License. Source code and additional information can be found on \url{http://sourceforge.net/projects/libgimli/}. \newpage

\minisec{BRUTUS}
\vspace{0.5cm}
BRUTUS is ETH's central resource for high-performance scientific computations. More than 140 commercial and open source applications are pre-installed in different versions and ready to use.\\
\\

\begin{tabular}{ll}
Official website: & \url{http://www.cluster.ethz.ch/index_EN}\\
Wiki pages: & \url{http://brutuswiki.ethz.ch/}  {\scriptsize (access only from ETH network or via VPN)}\\
Photo gallery: & \url{http://www.gallery.ethz.ch/brutus/}\\
Wikipedia: & \url{http://en.wikipedia.org/wiki/Brutus_cluster}\\
\end{tabular} 

\section{Getting started}
If you already have an account on BRUTUS (otherwise click \href{https://www1.ethz.ch/id/services/list/comp_zentral/cluster/brutus_acc_req_pre_EN}{here}), you can start by connecting to BRUTUS via your Linux or Mac OS X terminal.

\begin{lstlisting} 
ssh username@brutus.ethz.ch
\end{lstlisting}

If you are operating on Windows, you will need an external ssh client (e.g. \url{www.putty.org}). After typing your corresponding NETHZ password, you should see the following welcome screen:

\begin{lstlisting} 
      ____________________   ___
     /  ________   ___   /__/  /
    /  _____/  /  /  /  ___   /
   /_______/  /__/  /__/  /__/
   Eidgenoessische Technische Hochschule Zuerich
   Swiss Federal Institute of Technology Zurich
   ----------------------------------------------------------------------
                                   B R U T U S  C L U S T E R    CentOS 6


                                                http://brutuswiki.ethz.ch
                             NEW! -->  http://tinyurl.com/cluster-support
                                               cluster-support@id.ethz.ch
  

[username@brutus1 ~]$ 
\end{lstlisting}

\section{Necessary prerequisites}
Fortunately, (almost) all necessary prerequisites are either available on BRUTUS or shipped with BERT \& GIMLi. Software packages on BRUTUS are organized in modules and usually available in wide range of versions. You can list all the available modules by typing

\begin{lstlisting} 
module avail
\end{lstlisting}

For our purpose, we need to load the following modules:

\begin{lstlisting} 
module load gcc/4.6.2 python/2.7.2 boost/1.51.0 openblas/0.2.4_seq
\end{lstlisting}

To verify that everything was loaded correctly, you can type \textit{module list}.
\begin{lstlisting}
[username@brutus1 ~]$ module list
Currently Loaded Modulefiles:
  1) modules           2) gcc/4.6.2(4.6)    3) python/2.7.2(default:2.7)
  4) boost/1.51.0      5) openblas/0.2.4_seq(seq)
\end{lstlisting}

\textbf{Hint:} Modules that you use on a regular basis (like matlab for example) can be loaded automatically by adding the load command to your .bashrc file in your home directory.

\section{Building GIMLi}
\subsection{Downloading the source code}
The GIMLi source can be checked out via subversion:

\begin{lstlisting}
svn checkout svn://svn.code.sf.net/p/libgimli/code/ libgimli
\end{lstlisting}

\subsection{Building the dependencies}
Necessary dependencies are contained in the external folder. To build these, we type

\begin{lstlisting}
cd libgimli/trunk/external
make -j
\end{lstlisting}

\subsection{Installation}
After the initial repository checkout the configuration can be initialized by 

\begin{lstlisting}
cd ..
sh autogen.sh
\end{lstlisting}

Verify that the external matrix solvers that we have compiled in the previous section are found

\begin{lstlisting}
-------------------------------------------------
gimli Version 0.9.0.222 

ldl (local)             FOUND
amd (local)             FOUND
cholmod (local)         FOUND

triangle		FOUND

-lprocps        Not FOUND  # we can neglect that for the time being
(recommended for memwatch) 
GIMLi configuration complete for x86_64-unknown-linux-gnu
\end{lstlisting}

and that the system-wide gcc compiler and boost installations were detected correctly:

\begin{lstlisting}
C++ compiler:           /cluster/apps/gcc/gcc462/bin/g++-
OSTYPE:                 linux-gnu
CXXFLAGS:               -O2 -g -Wall -ansi -pedantic -pipe
CPPFLAGS:                -I/cluster/apps/boost/1.51.0/x86_64/serial/gcc_4.6.2/lib64/../include -I/cluster/apps/boost/1.51.0/x86_64/serial/gcc_4.6.2/include
Libraries:               -I/cluster/apps/boost/1.51.0/x86_64/serial/gcc_4.6.2/lib64 -L/cluster/home02/erdw/username/libgimli/trunk/external/lib -llapack -lblas -lcolamd -lcholmod -lamd -lldl -ltriangle -lboost_thread -lm -L/cluster/apps/boost/1.51.0/x86_64/serial/gcc_4.6.2/lib64 -lboost_system  
\end{lstlisting}

If all dependencies are met, we can start the build process by typing \textit{make}. Since this is potentially a slow operation on the login node alone, you may consider to submit this as a BRUTUS job using the load sharing facility  and the \textit{bsub} command (for more information see \href{http://brutuswiki.ethz.ch/brutus/Using_the_batch_system#Basic_job_submission}{this} tutorial).

\begin{lstlisting}
bsub -I make -j
\end{lstlisting}

If an error relating to the boost library occurs, try to specify the boost paths explicitly and repeat the procedure:

\begin{lstlisting}
export LIBS="-L$BOOST_LIBRARYDIR -lboost_system"
export CPPFLAGS="$CPPFLAGS -I$BOOST_LIBRARYDIR/../include"
export LDFLAGS="$LDFLAGS -I$BOOST_LIBRARYDIR"
make distclean
sh autogen.sh
bsub -I make -j
\end{lstlisting}

Finally you can type

\begin{lstlisting}
make install
\end{lstlisting}

The compiled binaries and the library files can be find in \textit{bin/} and \textit{lib/}, respectively. Make sure that the appropriate system variables point to these paths.

\begin{lstlisting}
export PATH="$PATH:/cluster/home02/erdw/username/libgimli/trunk/bin"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/cluster/home02/erdw/username/libgimli/trunk/lib"
\end{lstlisting}

To verify if everything is working correctly, we can try to call the traveltime inversion command line application of GIMLi for example

\begin{lstlisting}
ttinv
\end{lstlisting}

and should see the usage description without an error message. If so, we can continue to build the BERT program.

\section{Building BERT}
\subsection{Downloading the source code}
You can get the source code via svn
\begin{lstlisting}
svn co --username=nethz-id https://code.vis.ethz.ch/svn/bert_on_brutus/ .
\end{lstlisting}
Note that this repository is only accessible for students of the Earth Science Department (D-ERDW) of ETH. In other cases, please contact the \href{http://www.resistivity.net/index.php?id=12&type=1}{software developers}.

\subsection{Installation}
GIMLi is the only dependency for BERT. To make sure that it is detected correctly, verify that the programs are installed in parallel (./libgimli/trunk/ and ./bert/trunk/ or ./libgimli/ and ./bert/). Subsequently, the build process follows the same commands as in the previous section

\begin{lstlisting}
cd bert/trunk
sh autogen.sh
bsub -I make -j
make install
\end{lstlisting}

Again make sure that the /bin, /apps and /lib folders are known to your system's \textit{PATH} and \textit{LD\_LIBRARY\_PATH} variables.

If so, you should be able to call the command line inversion routine for example.

\begin{lstlisting}
dcinv --version
\end{lstlisting}

\section{Building DCFemLib}
Mesh preparation, creation and conversion of BERT depends on the \href{http://www.resistivity.net/index.php?id=dcfemlib&type=1}{DCFemLib} Library and its dependencies. We can copy (or link) the necessary dependencies from GIMLi

\begin{lstlisting}
cp -r libgimli/trunk/external/lib dcfemlib/trunk/external/
cp -r libgimli/trunk/external/SuiteSparse/* dcfemlib/trunk/external/
cp -r libgimli/trunk/external/triangle dcfemlib/trunk/external/
cd dcfemlib/trunk/external/triangle/ && make triangle # if not compiled already
cp dcfemlib/trunk/external/lib/libtriangle.a dcfemlib/trunk/external/triangle/
cd dcfemlib/trunk/ && sh autogen.sh
\end{lstlisting}

If boost, cholmod and triangle are detected, you can proceed with 

\begin{lstlisting}
bsub -I make -j
make install
cd apps/ && make -j && make install
\end{lstlisting}

If your system variables point to the appropriate paths, then you should be able to call \textit{meshconvert} for instance. If you intend to work with three-dimensional tetrahedral meshes, the last thing you need to install is TetGen (\url{www.tetgen.org}). Go to a directory, which is known to your system and download and install using the following:

\begin{lstlisting}
wget http://tetgen.org/files/tetgen1.4.3.tar.gz
tar -xzf tetgen1.4.3.tar.gz 
cd tetgen1.4.3/ && make
\end{lstlisting}

\vspace{1cm}
{\Large That should be it! For usage instructions of BERT, have a look at the tutorial contained in \textit{bert/trunk/doc/tutorial}.}
\vspace{1cm}

\begin{lstlisting}
cd bert/trunk/doc/tutorial && pdflatex bert-tutorial.tex # You will probably need to copy the resulting bert-tutorial.pdf to your local machine to view it.
\end{lstlisting}


\end{document}          
