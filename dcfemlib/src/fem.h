// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef BASEFEM__H
#define BASEFEM__H

#include "dcfemlib.h"
#include "datamap.h"
using namespace DCFEMLib;

#include "mesh2d.h"
#include "mesh3d.h"
using namespace MyMesh;

#include "myvec/vector.h"
#include "myvec/matrix.h"

/* some global tool-functions */
vector <int> findSourcesFromFile( const BaseMesh & mesh, const string & fname );
int solveAnalytical( const BaseMesh & mesh, const RealPos & sourcePos, RVector & solution, double k = 0.0, 
		     SpaceConfigEnum config = MIRRORSOURCE );
double solveAnalytical( const RealPos & queryPos, const RealPos & sourcePos, double k = 0.0,
			SpaceConfigEnum config = MIRRORSOURCE );
RVector solveAnalytical( const vector < RealPos > & queryPos, const RealPos & sourcePos, double k = 0.0, 
			 SpaceConfigEnum config = MIRRORSOURCE );
RVector solveAnalytical( const BaseMesh & mesh, const RealPos & sourcePos, double k = 0.0, 
			 SpaceConfigEnum config = MIRRORSOURCE  );

RealPos solveAnalyticalGrad( const RealPos & queryPos, const RealPos & sourcePos, double k = 0.0 );
vector < RealPos > solveAnalyticalGrad( const vector < RealPos > & queryPos, const RealPos & sourcePos, double k = 0.0 );
vector < RealPos > solveAnalyticalGrad( const BaseMesh & mesh, const RealPos & sourcePos, double k = 0.0 );

double boundaryBetaDC2D( const RealPos & source1, const RealPos & source2, BaseElement & boundary, SpaceConfigEnum config );
double boundaryBetaDC2D5( const RealPos & source1, BaseElement & boundary, SpaceConfigEnum config, double k );
double boundaryAlphaDC3D( const RealPos & sourcePos, BaseElement & boundary, SpaceConfigEnum config );
DLLEXPORT void initKWaveList( const BaseMesh & mesh, 
			      vector < double > & kValues, vector < double > & weights, 
			      const vector < RealPos > & sources, bool verbose = false );

DLLEXPORT void initKWaveList( const BaseMesh & mesh, 
			      vector < double > & kValues, vector < double > & weights, bool verbose = false );

int applyHomogDirichletBC( RDirectMatrix & S, RVector & rhs, const set < long > & nodesSet, bool verbose = false );
int applyInHomogDirichletBC( RDirectMatrix & S, RVector & rhs, const map < long, double > & nodesMap, bool verbose );

class DLLEXPORT BaseFEM{
public:

  BaseFEM( BaseMesh & mesh );
  virtual ~BaseFEM();
  
  //** START GET-SET Functions
  //  BaseMesh & mesh() { return _mesh; }

  /*! Returns a reference to the used mesh */
  BaseMesh & mesh() const { return *mesh_; }
  /*! Sets the used FEM-mesh */
  void setMesh( const BaseMesh & mesh ){ *mesh_ = mesh; }
  /*! Returns the "right hand side" vector */ 
  RVector & rhs() const { return *rhs_; }
  /*! Sets the "right hand side" vector */ 
  void setRHS( const RVector & rhs ){ *rhs_ =  rhs; rhs_given = true; }
  /*! Returns the selected kind of solver.*/
  SolverEnum solver(){ return _solver; }
  /*! Sets kind of solver for the large system of equations. See \ref SolverEnum. Default = CG */
  void setSolver( SolverEnum solver ){ _solver = solver; }
  /*! Returns TRUE if no informations about the solving-status are outputed.*/
  bool solverVerbose(){ return _solver_verbose; }
  /*! Sets TRUE the solving-status are outputed. So far only the number of iterations are printed on screen. Default = TRUE.*/
  void setSolverVerbose( bool verbose ){ _solver_verbose = verbose; }
  /*! Returns the aborting border for the solution. Default = 1E-15 */
  double solverEpsilon(){ return _solver_epsilon; }
  /*! Sets the aborting border for the solution. Default = 1E-15 */
  void setSolverEpsilon( double epsilon ){ _solver_epsilon = epsilon; }

  /*! Main FEM-function for the pde-calculation returns the solution of the
    linear system of Equation Sx=b; and manage the construction of the stiffness-matrix S */
  virtual RVector calculate();

  /*! Applies in Matrix S the homogen Dirichlet Boundary Conditions on
    each index in nodesVector. nodesVector<long> musst be a vector of
    node_indicies on which die homogen Dirichlet BC are defined. hom
    Dirichlet BC means: u = 0. */
  int applyHomDirichletBC( RDirectMatrix & S, const set < long > & nodesSet );

  /*! Applies in Matrix S the Dirichlet Boundary Condition on each
    index in nodesMap. nodesMap<long, map> musst be a vector of
    node_indicies at which die homogen Dirichlet BC are defined and a
    double value for u_0 at this node. Dirichlet BC means: u = u_0. */
  int applyDirichletBC( RDirectMatrix & S, const map < long, double > & nodesMap );
  
  double energyNorm( RVector & e );

  RDirectMatrix & stiffnessMatrix() { return * S_; }

  //  virtual int solveAnalytical( RVector & solution ) = 0;

  void setSources( vector< int > & sources ){ 
    _sources.clear();    
    for ( int i = 0, imax = sources.size(); i < imax; i ++ ){
      if ( sources[ i ] > -1 ) _sources.push_back( sources[ i ] );
      else break; 
    }
  }

  vector< int > & sources(){ return _sources;}

  virtual int generateRHSVector() = 0;
  virtual void setMatrixUpdate( bool update ){ matrixUpdate_= update; }

  void setVerbose( bool verbose ){ verbose_ = verbose; }
  bool verbose( ){ return verbose_;}

  virtual int compileMatrix( RDirectMatrix & S );

  double dropTolerance() const { return dropTol_; }
  void setDropTolerance( double dropTol ) { dropTol_ = dropTol; }

protected:
  /*! Manage the construction of the stiffness-matrix S. Is called by calculate. */
  virtual int compileBasisMatrix( RDirectMatrix & S ) = 0;
  virtual int compileBasisMatrixBoundary( RDirectMatrix & S, const RealPos & sourcePos = RealPos(), 
					  double rhoBoundary_ = 0.0 ) = 0;
  
  BaseMesh * mesh_;
  RVector * rhs_;
  SolverEnum _solver;
  bool _solver_verbose;
  double dropTol_;
  double _solver_epsilon;

  vector < RDirectMatrix * > SList;
  vector < int > _sources;
  bool rhs_given;
  bool matrixUpdate_;
  RDirectMatrix * S_;

  bool verbose_;
};

class DLLEXPORT DCGeoElectricFEM : public BaseFEM {
public:
  DCGeoElectricFEM( BaseMesh & mesh ) : BaseFEM( mesh ){
    current_= 1.0; 
    rhoBG_= 0.0; 
    spaceConfig_ = HALFSPACE;
    k_ = 0.0;
  }
  virtual ~DCGeoElectricFEM(){}
//   virtual int solveAnalytical( RVector & solution, int source, double rho, bool verbose = true );
//   virtual int solveAnalytical2D( RVector & solution, double rho );
//   virtual int solveAnalytical2D5( const RealPos & sourcePos, RVector & solution );

//  virtual int solveAnalytical( RVector & );
  virtual int generateRHSVector();
  virtual int compileBasisMatrix( RDirectMatrix & S );
  virtual int compileBasisMatrixBoundary( RDirectMatrix & S, const RealPos & sourcePos = RealPos(),  
					  double rhoBoundary_ = 0.0);
  inline void insertCurrent( int source, double current ){ rhs_->at( source + 1 ) = current; }
  inline void setRhoBoundary( double rho0 ){ rhoBG_ = rho0; }
  inline void setRhoBackground( double rho0 ){ rhoBG_ = rho0; }
  inline void setSpaceConfig( SpaceConfigEnum config ) { spaceConfig_ = config; }
  inline SpaceConfigEnum spaceConfig( ) { return spaceConfig_; }

  RVector calculate2D5( const string & dataFileBody = NOT_DEFINED, int elektrode = 0, bool verbose = true );
  double rhoBackground() { return rhoBG_; }
  void setSingleSourceNode( int i ){ 
    _sources.clear();    
    _sources.push_back( i );
  }

  double k() const { return k_;}
  void setK( double k ){ k_ = k; }

  map < long, double > dirichletMap() { return dirichletMap_; }
protected:
  double current_;
  double rhoBG_;
  double k_;
  SpaceConfigEnum spaceConfig_;
  map < long, double > dirichletMap_;
};

class DLLEXPORT DCGeoElectricSRFEM : public DCGeoElectricFEM {
public:
  DCGeoElectricSRFEM( BaseMesh & mesh ) : DCGeoElectricFEM( mesh ){
    Srhs_ = new RDirectMatrix( mesh.nodeCount() ) ;
  }

  virtual ~DCGeoElectricSRFEM(){
    delete Srhs_;
  }
  virtual RVector calculate();
  virtual int generateRHSVector( const RVector & potPrim );
  virtual int setPrimaerPotential( const RVector & potPrim );

protected:

  RDirectMatrix * Srhs_;
};

class DLLEXPORT MultiElectrodeModelling {
  public:
  MultiElectrodeModelling( DCGeoElectricFEM * fem, 
			  double rhoBackground = -1.0,
			  bool verbose = false ) 
    : fem_( fem ), rhoBackground_( rhoBackground ), verbose_( verbose ){
    
    electrodeNodeIndex_ = fem->mesh().getAllMarkedNodes( -99 );
    vecSourcePos_ = fem_->mesh().positions( electrodeNodeIndex_ );
    setDefaultValues();
  }
  MultiElectrodeModelling( DCGeoElectricFEM * fem, 
			  const vector < int > & electrodeNodeIndex, 
			  double rhoBackground = -1.0,
			  bool verbose = false ) 
    : fem_( fem ), electrodeNodeIndex_( electrodeNodeIndex ), 
      rhoBackground_( rhoBackground ), verbose_( verbose ){
    vecSourcePos_ = fem_->mesh().positions( electrodeNodeIndex );
    setDefaultValues();
  }
  MultiElectrodeModelling( DCGeoElectricFEM * fem, 
			   const vector < RealPos > & vecSourcePos, 
			   double rhoBackground = -1.0,
			   bool verbose = false ) 
    : fem_( fem ), vecSourcePos_( vecSourcePos ), rhoBackground_( rhoBackground ), verbose_( verbose ){
    setDefaultValues();
  }
  void setDefaultValues(){
    dataFileBody_ = NOT_DEFINED;
    haveSubSolution_ = false;
    analytical_ = false;
    referenceElectrode_ = false;
    dipole_ = false;
    circle_ = false;
    hold_ = true;
    solverType_ = TAUCSWRAPPER;
    dropTol_ = 0.0;
    epsilon_ = 1e-6;
    referenceNodeIdx_ = -1;
    fromElectrode_ = -1;
    toElectrode_ = -1;
  }
  virtual ~MultiElectrodeModelling( ){ }

  RealPos findAverageSource() const {  return averagePosition( vecSourcePos_ ); }

  //  virtual int calculate( vector < RVector > & solution, int from = -1, int to = -1, bool hold = false, bool dummy = false );
  //  virtual int calculate( int from, int to );
  //  virtual int calculate2D5CircleK( vector < RVector > & solution, double kVal = 0, int k = -1);
  virtual int calculateK( vector < RVector > & solution, double kVal = 0, int k = -1);
  //  virtual int calculate2D5Circle( vector < RVector > & solution );
  virtual int calculate( vector < RVector > & solution );
  //  virtual int calculateCylinder( vector < RVector > & solution );

  int electrodeCount() const { return vecSourcePos_.size(); }

  string dataBody() const { return dataFileBody_; }
  void setDataBody( const string & dataFileBody) { dataFileBody_ = dataFileBody; }

  double rhoBackground( ) const { return rhoBackground_; }
  void setRhoBackground( double rho ){ rhoBackground_ = rho; }

  bool circleGeometry( ) const { return circle_; }
  void setCircleGeometry( bool circle ){ circle_= circle; if ( circle_ ) dipole_ = true; }

  bool dipoleSources( ) const { return dipole_; }
  void setDipoleSources( bool dipole ) { dipole_ = dipole; } 

  bool analytical( ) const { return analytical_; }
  void setAnalytical( bool analytical ) {
    analytical_ = analytical; 
    if ( analytical_ ) solverType_ = ANALYT;
  }
  
  bool holdInMemory() const { return hold_; }
  void setHoldInMemory( bool hold ){ hold_ = hold; }

  bool isWithReferenceElectrode() const { return referenceElectrode_; }
  void withReferenceElectrode( bool with ) { 
    referenceElectrode_ = with ; 
    if ( with ){
      vector< int > refSources = fem_->mesh().getAllMarkedNodes( REFERENCELECTRODE_MARKER );
      if ( refSources.size() > 0 ){
	setReferenceNodeIdx( refSources[ 0 ] );
      } else {
	cerr << WHERE_AM_I << " no node with reference node.marker() == " << REFERENCELECTRODE_MARKER << " found " << endl;
	exit( 0 );
      }
    }
  }
  
  void setReferenceNodeIdx( int nodeIdx ) { referenceNodeIdx_ = nodeIdx; }
  int referenceNodeIdx( ) const { return referenceNodeIdx_; }
  

  SolverEnum solver( ) const { return solverType_; }
  void setSolver( SolverEnum solverType ){ 
    solverType_ = solverType; 
    if ( solverType_ == ANALYT ) analytical_ = true; 
  }

  void setSolverDropTol( double dropTol ){ dropTol_ =  dropTol; }
  void setSolverEpsilon( double epsilon ){ epsilon_ = epsilon; }

  void setpSubSolution( vector< RVector > & subSolutionMatrix ){
    haveSubSolution_ = true;
    subSolution_ = &subSolutionMatrix;
  }

  vector < double > kValues() const { return kValues_; }

  void setFromToElectrode( int from, int to ){
    fromElectrode_ = from;
    toElectrode_ = to;
  }

protected:

  DCGeoElectricFEM * fem_;
  string dataFileBody_;
  vector < int > electrodeNodeIndex_;
  vector < RealPos > vecSourcePos_;
  vector < RVector > * subSolution_;
  bool  haveSubSolution_;
  double rhoBackground_;
  bool referenceElectrode_;
  int referenceNodeIdx_;

  bool verbose_;
  bool dipole_;
  bool circle_;
  bool analytical_;
  bool hold_;
  SolverEnum solverType_;
  vector < double > kValues_;
  double dropTol_;
  double epsilon_;
  int fromElectrode_;
  int toElectrode_;
};

class DLLEXPORT MultiElectrodeModellingSingularityRemoval : public MultiElectrodeModelling{
  public:
  MultiElectrodeModellingSingularityRemoval( DCGeoElectricFEM * fem, 
					     double rhoBackground = -1.0,
					     bool verbose = false )
    : MultiElectrodeModelling( fem, rhoBackground, verbose ) {
    primaryPotFileBody_ = NOT_DEFINED;
    havePrimPot_ = false;
    collectOnly_ = false;
  }

  MultiElectrodeModellingSingularityRemoval( DCGeoElectricFEM * fem, 
					     const vector < int > & electrodeNodeIndex, 	      
					     double rhoBackground = -1.0,
					     bool verbose = false ) 
    : MultiElectrodeModelling( fem,  electrodeNodeIndex, rhoBackground, verbose ){
    primaryPotFileBody_ = NOT_DEFINED;
    havePrimPot_ = false;
    collectOnly_ = false;
  }
  MultiElectrodeModellingSingularityRemoval( DCGeoElectricFEM * fem, 
					     const vector < RealPos > & vecSourcePos, 
					     double rhoBackground = -1.0,
					     bool verbose = false ) 
    : MultiElectrodeModelling( fem, vecSourcePos, rhoBackground, verbose ){
    primaryPotFileBody_ = NOT_DEFINED;
    havePrimPot_ = false;
    collectOnly_ = false;
  }

  virtual ~MultiElectrodeModellingSingularityRemoval( ){ }

//   virtual int calculate( vector < RVector > & solution, int from = -1, int to = -1, 
// 			 bool returnTotal = false, bool hold = false);

//   int calculateForInversion( vector < RVector > & solution, int kIdx = -1, bool verbose = true );
//   int calculateForInversion2D5( vector < RVector > & solutionMat );

  void setPrimaryPotFileBody( const string & primaryPotFileBody ){ 
    primaryPotFileBody_ = primaryPotFileBody; 
  }

  //  int calculate2D5CircleK( vector < RVector > & solution, double kVal = 0, int k = -1);
  int calculateK( vector < RVector > & solution, double kVal = 0, int k = -1);

  double rhoBackground( ){ return rhoBackground_; }
  void setRhoBackground( double rho ){ rhoBackground_ = rho; }
  double findRhoBackground();
  vector <double> findIndividualRhoBackground();

  void setpPrimPotential( vector< RVector > & primPot ){
    havePrimPot_ = true;
    primPot_ = &primPot;
  }

  void setDataMap( DataMap & map, bool collectOnly = true ){ dataMap_ = &map; collectOnly_ = collectOnly; }
  bool haveCollect() const { return collectOnly_; }
protected:

  void addResistivity_( double rho );
  void subResistivity_( double rho );
  string primaryPotFileBody_;
  vector < RVector > * primPot_;
  bool havePrimPot_;
  bool collectOnly_;
  DataMap * dataMap_;
};

#endif // BASEFEM__H

/*
$Log: fem.h,v $
Revision 1.27  2007/12/13 18:42:16  carsten
*** empty log message ***

Revision 1.26  2007/08/02 15:22:15  carsten
*** empty log message ***

Revision 1.25  2007/03/07 16:38:14  carsten
*** empty log message ***

Revision 1.24  2006/08/15 14:04:38  carsten
*** empty log message ***

Revision 1.23  2006/07/27 15:49:50  carsten
*** empty log message ***

Revision 1.22  2005/10/17 15:10:17  carsten
*** empty log message ***

Revision 1.21  2005/10/12 12:35:21  thomas
*** empty log message ***

Revision 1.20  2005/10/06 17:38:36  carsten
*** empty log message ***

Revision 1.19  2005/10/04 11:07:42  carsten
*** empty log message ***

Revision 1.18  2005/09/30 18:09:40  carsten
*** empty log message ***

Revision 1.17  2005/09/23 11:24:41  carsten
*** empty log message ***

Revision 1.16  2005/08/17 10:59:38  carsten
*** empty log message ***

Revision 1.15  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.14  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.13  2005/07/13 13:36:49  carsten
*** empty log message ***

Revision 1.12  2005/03/29 16:57:46  carsten
add inversion tools

Revision 1.11  2005/03/24 14:39:03  carsten
*** empty log message ***

Revision 1.10  2005/02/22 21:51:43  carsten
*** empty log message ***

Revision 1.9  2005/02/15 14:38:03  carsten
*** empty log message ***

Revision 1.8  2005/02/15 11:24:00  carsten
*** empty log message ***

Revision 1.7  2005/02/07 13:33:13  carsten
*** empty log message ***

Revision 1.6  2005/01/13 21:12:15  carsten
*** empty log message ***

Revision 1.5  2005/01/13 20:10:42  carsten
*** empty log message ***

Revision 1.4  2005/01/12 20:32:40  carsten
*** empty log message ***

Revision 1.3  2005/01/11 19:12:26  carsten
*** empty log message ***

Revision 1.2  2005/01/07 15:27:30  carsten
*** empty log message ***

Revision 1.1  2005/01/06 20:28:30  carsten
*** empty log message ***

*/
