// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include "electrodeconfig.h"
// #include "vektor/vektor.h"
#include "meshadds.h"
#include "mesh2d.h"
// #include "plane.h"

#include <list>
#include <vector>
#include <sstream>

using namespace std;
using namespace MyVec;


int loadElectrodeList( vector < RealPos > & electrodeList, const string & fname ){
  electrodeList.clear();
  DO_NOT_USE // columncount ersetzen
  int columnCount = countColumns( fname );
  fstream file; if ( !openInFile( fname, & file ) ) {  return 0; }

  double x = 0.0, y = 0.0, z = 0.0;  int iDummy = 0;
  ElectrodeConfigVector profiles;
  switch( columnCount ){
  case 1: // ElectrodeConfigProfile
    profiles.load( fname );
    electrodeList = profiles.electrodePositions();
    break;
  case 2: // Simple List: x y
    while( file >> x >> y ){
      electrodeList.push_back( RealPos( x, y ) );
    }
  case 3: // Simple List: x y z
    while( file >> x >> y >> z ){
      electrodeList.push_back( RealPos(x, y, z) );
    }
  case 4: // MyMesh mode-file: x y z marker
    while( file >> x >> y >> z >> iDummy ){
      electrodeList.push_back( RealPos(x, y, z) );
    }
  default:
    cerr << WHERE_AM_I << ": default not defined " << endl;
    exit( 1 );
  }
  file.close();
  return 1;
}

ostream & operator << ( ostream & str, const ElectrodeConfig & p ){
  str << "(" << p.a() << ", " << p.b() << " -- " << p.m() << ", " << p.n() << ") "
      << "rhoA = " <<  p.rhoA()
      << " K = " <<  p.k()
      << " dU = " <<  p.u();
  return str;
}

void ElectrodeConfigVector::setElectrodePositions( const DataMap & dataMap ){
  electrodePositions_.clear();
  for ( size_t i = 0; i < dataMap.size(); i ++ ){
    electrodePositions_.push_back( dataMap.electrodePosition( i ) );
  }
}

int ElectrodeConfigVector::load( const string & fileName, const string & dataFormatString, bool fromOne ){
  //** noch eine Ignore option f�r die Tokens, z.Bsp f�r Argus

  this->clear();
  electrodePositions_.clear();

  fstream file; if ( !openInFile( fileName, & file ) ) return 0;
  vector < string > row; row = getNonEmptyRow( file );
  if ( row.size() != 1 ) { cerr << WHERE_AM_I << " cannot determine data format. " << row.size() << endl; return 0; }

  int nElectrodes = toInt( row[ 0 ] );

  RVector x( nElectrodes, 0.0 ), y( nElectrodes, 0.0 ), z( nElectrodes,0.0 );
  RVector h( nElectrodes, 0.0 ), d( nElectrodes, 0.0 );

  vector < string > format( getSubstrings( "x y z" ) );

  char c; file.get( c );
  if ( c == '#' ) {
    format = getRowSubstrings( file );
    if ( format[ 0 ][ 0 ] != 'x' && format[ 0 ][ 0 ] != 'X' ){
      format = getSubstrings( "x y z" );
    }
  }
  file.unget();

  for ( int i = 0; i < nElectrodes; i ++ ){
    row = getNonEmptyRow( file );
    if ( row.size() == 0 ) {
      cerr << fileName << ": To few electrode data. " << nElectrodes << " Electrodes expected and " << i << " found." << endl;
      exit( 1 );
    }
    for ( uint j = 0; j < row.size(); j ++ ){
      if ( j == format.size() ) break;

      if ( format[ j ] == "x" || format[ j ] == "X" ) x[ i ] = toDouble( row[ j ] );
      else if ( format[ j ] == "y" || format[ j ] == "Y" ) y[ i ] = toDouble( row[ j ] );
      else if ( format[ j ] == "z" || format[ j ] == "Z" ) z[ i ] = toDouble( row[ j ] );
      else if ( format[ j ] == "-z" || format[ j ] == "-Z" ) z[ i ] = -toDouble( row[ j ] );
      else if ( format[ j ] == "h" || format[ j ] == "H" ) h[ i ] = toDouble( row[ j ] );
      else if ( format[ j ] == "-h" || format[ j ] == "-H" ) h[ i ] = -toDouble( row[ j ] );
      else if ( format[ j ] == "d" || format[ j ] == "D" ) d[ i ] = toDouble( row[ j ] );
      else if ( format[ j ] == "-d" || format[ j ] == "-D" ) d[ i ] = -toDouble( row[ j ] );
      else {
	cerr << WHERE_AM_I << " format description not defined: format[ " << j << " ] = " << format[ j ] << endl;
      }
    }
  }

  this->setElectrodeDepths( d );
  if ( !isZero( d ) ){ // bury electrodes present. Q&D
    this->setBuryElectrodesPresent( true );
  }
  for ( int i = 0 ; i < nElectrodes; i ++ ){
    electrodePositions_.push_back( RealPos( x[ i ], y[ i ], z[ i ] + h[ i ] ).round( 1e-14 ) );
  }

//   for ( int i = 0 ; i < nElectrodes; i ++ ){
//     row = getNonEmptyRow( file );
//     switch ( row.size() ){
//     case 1:
//       x[ i ] = toDouble( row[ 0 ] );
//       y[ i ] = 0.0;
//       z[ i ] = 0.0;
//       break;
//     case 2:
//       x[ i ] = toDouble( row[ 0 ] );
//       y[ i ] = toDouble( row[ 1 ] );
//       z[ i ] = 0.0;
//       break;
//     case 3:
//       x[ i ] = toDouble( row[ 0 ] );
//       y[ i ] = toDouble( row[ 1 ] );
//       z[ i ] = toDouble( row[ 2 ] );
//       break;
//     default:
//       cerr << WHERE_AM_I << " cannot determine electrode-format for row " << i << " " << row.size() << endl;
//       return 0;
//       break;
//     }
//   }

//   if ( isZero( y ) ) {
//     y = z;
//     z = RVector( nElectrodes, 0.0 );
//   }

  // ** start reading data ;
  row = getNonEmptyRow( file );
  if ( row.size() != 1 ) { cerr << WHERE_AM_I << " cannot determine data size. " << row.size() << endl; return 0; }

  int nData = toInt( row[ 0 ] );
  fromOne = true;

  // valid keywords a b m n rhoa err ip u r i k

  // default a b m n rhoa err ip u i
  format = getSubstrings( dataFormatString );

  file.get( c );
  if ( c == '#' ) {
    format = getRowSubstrings( file );
    if ( format[0] != "a" && format[0] != "A" && format[0] != "c1" && format[0] != "C1" ){
      format = getSubstrings( dataFormatString );
    }
  }
  file.unget();

  inputFormatString_ = "";
  for ( uint i = 0; i < format.size(); i ++) inputFormatString_ += format[ i ] + " ";

//   cout << inputFormatString_ << endl;
//   for ( int i = 0; i < format.size(); i ++ )  cout << format[i] << endl;
//   exit(0);

  for ( int data = 0; data < nData; data ++ ){
    int a = 0, b = 0, m = 0, n = 0;
    double rhoa = 0.0, err = 0.0, ip = 0.0, iperr = 0.0, u = 0.0, r = 0.0, curr = 1.0, k = 0.0, t = 1.0;

    row = getNonEmptyRow( file );
//     if ( row.size() > format.size() ){
//       cerr << WHERE_AM_I << " format description to short: found row.size() = "
// 		 << row.size() << " format.size() = " << format.size() << endl;
//     }
    if ( row.size() == 0 ) {
      cerr << fileName << ": To few data. " << nData << " data expected and " << data << " data found." << endl;
      exit( 1 );
    }
    for ( uint j = 0; j < row.size(); j ++ ){
      if ( j == format.size() ) break;

      //std::cout << j << " " << format[ j ] << std::endl;

      if (      format[ j ] == "a"         || format[ j ] == "A" ||
	        format[ j ] == "c1"        || format[ j ] == "C1" )         a = toInt( row[ j ] ) - fromOne;
      else if ( format[ j ] == "b"         || format[ j ] == "B" ||
		format[ j ] == "c2"        || format[ j ] == "C2" )	    b = toInt( row[ j ] ) - fromOne;
      else if ( format[ j ] == "m"         || format[ j ] == "M" ||
		format[ j ] == "p1"        || format[ j ] == "P1" )         m = toInt( row[ j ] ) - fromOne;
      else if ( format[ j ] == "n"         || format[ j ] == "N" ||
		format[ j ] == "p2"        || format[ j ] == "P2" )         n = toInt( row[ j ] ) - fromOne;
      else if ( format[ j ] == "rhoa"      || format[ j ] == "Rhoa" ||
		format[ j ] == "rho_a"     || format[ j ] == "Rho_a" ||
		format[ j ] == "ra"        || format[ j ] == "Ra" )         rhoa = toDouble( row[ j ] );
      else if ( format[ j ] == "rho"       || format[ j ] == "Rho" ||
	        format[ j ] == "r"         || format[ j ] == "R" ||
		format[ j ] == "r(Ohm)"    || format[ j ] == "R(Ohm)" ||
		format[ j ] == "imp"       || format[ j ] == "Imp" ||
		format[ j ] == "z"         || format[ j ] == "Z" ||
		format[ j ] == "U/I"       || format[ j ] == "u/i" )        r = toDouble( row[ j ] );
      else if ( format[ j ] == "err"       || format[ j ] == "Err" )       err = toDouble( row[ j ] );
      else if ( format[ j ] == "err(%)"    || format[ j ] == "Err(%)" )    err = toDouble( row[ j ] ) / 100.0;
      else if ( format[ j ] == "err/%"     || format[ j ] == "Err/%" )     err = toDouble( row[ j ] ) / 100.0;
      else if ( format[ j ] == "sd(%)"     || format[ j ] == "SD(%)" )     err = toDouble( row[ j ] ) / 100.0;
      else if ( format[ j ] == "ip"        || format[ j ] == "IP" )        ip = toDouble( row[ j ] );
      else if ( format[ j ] == "iperr"     || format[ j ] == "IPerr" )     iperr = toDouble( row[ j ] );
      else if ( format[ j ] == "u"         || format[ j ] == "U" )         u = toDouble( row[ j ] );
      else if ( format[ j ] == "u(V)"      || format[ j ] == "U(V)" )      u = toDouble( row[ j ] );
      else if ( format[ j ] == "u/V"       || format[ j ] == "U/V" )       u = toDouble( row[ j ] );
      else if ( format[ j ] == "u(mV)"     || format[ j ] == "U(mV)" )     u = toDouble( row[ j ] ) / 1000.0;
      else if ( format[ j ] == "u[mV]"     || format[ j ] == "U[mV]" )     u = toDouble( row[ j ] ) / 1000.0;
      else if ( format[ j ] == "u/mV"      || format[ j ] == "U/mV" )      u = toDouble( row[ j ] ) / 1000.0;
      else if ( format[ j ] == "v/mV"      || format[ j ] == "V/mV" )      u = toDouble( row[ j ] ) / 1000.0;
      else if ( format[ j ] == "i"         || format[ j ] == "I" )         curr = toDouble( row[ j ] );
      else if ( format[ j ] == "i(A)"      || format[ j ] == "I(A)" )      curr = toDouble( row[ j ] );
      else if ( format[ j ] == "i/A"       || format[ j ] == "I/A" )       curr = toDouble( row[ j ] );
      else if ( format[ j ] == "i(mA)"     || format[ j ] == "I(mA)" )     curr = toDouble( row[ j ] ) / 1000.0;
      else if ( format[ j ] == "i/mA"      || format[ j ] == "I/mA" )      curr = toDouble( row[ j ] ) / 1000.0;
      else if ( format[ j ] == "i[mA]"     || format[ j ] == "I[mA]" )     curr = toDouble( row[ j ] ) / 1000.0;
      else if ( format[ j ] == "k"         || format[ j ] == "K" )         k = toDouble( row[ j ] );
      else if ( format[ j ] == "t"         || format[ j ] == "T" )         t = toDouble( row[ j ] );
      else if ( format[ j ] == "t(min)"    || format[ j ] == "T(min)" )    t = toDouble( row[ j ] ) * 60.0;
      else if ( format[ j ] == "zeit(min)" || format[ j ] == "Zeit(min)" ) t = toDouble( row[ j ] ) * 60.0;
      else if ( format[ j ] == "time(min)" || format[ j ] == "time/min" )  t = toDouble( row[ j ] ) * 60.0;
      else {
	cerr << WHERE_AM_I << " format description not defined: format[ " << j << " ] = " << format[ j ] << endl;
      }

    }

    if ( curr != 0.0 && u != 0.0 ) r = u / curr;

    if ( a > nElectrodes || b > nElectrodes || m > nElectrodes || n > nElectrodes ){
      cerr << WHERE_AM_I << " Warning! Number of electrodes = "  << nElectrodes
	   << "; data = " << data << ", a = " << a << " b = " << b << " m = " << m << " n = " << n << endl;
    }

    if ( a != b && m != n ){
      if ( a > -1 && a == m ) continue;
      if ( a > -1 && a == n ) continue;
      if ( b > -1 && b == m ) continue;
      if ( b > -1 && b == n ) continue;

      if ( isnan( u ) || isinf( u ) ){
	cerr << WHERE_AM_I << "Warning! u ( " << data << " ) = " << u << " ignoring." <<  endl;
	continue;
      }
      if ( isnan( r ) || isinf( r ) ){
	cerr << WHERE_AM_I << "Warning! r ( " << data << " ) = " << r << " ignoring." <<  endl;
	continue;
      }
      if ( isnan( err ) || isinf( err ) ){
	cerr << WHERE_AM_I << "Warning! err ( " << data << " ) = " << err << " ignoring." <<  endl;
	continue;
      }
      this->push_back( ElectrodeConfig( a, b, m, n ) );
      this->back().setRhoA( rhoa );
      this->back().setK( k );
      this->back().setU( u );
      this->back().setI( curr );
      this->back().setIp( ip );
      this->back().setIperr( iperr );
      this->back().setR( r );
      this->back().setT( t );
      this->back().setErr( err );

    }
  }
  if ( verbose_ ) cout << "found: " << nElectrodes << " electrodes and " << nData << " data" << endl;

  if ( electrodePositions_.size() == 0 ){
  if ( verbose_ ) cout << "create Dummy electrodes" << endl;
    createDummyElectrodes();
  }

  file.close();
  return 1;
}

void ElectrodeConfigVector::createDummyElectrodes(){
  electrodePositions_.clear();
  set < int > electrodeCounter;
  for ( size_t i = 0; i < this->size(); i ++ ){
    electrodeCounter.insert( (*this)[i].a() );
    electrodeCounter.insert( (*this)[i].b() );
    electrodeCounter.insert( (*this)[i].m() );
    electrodeCounter.insert( (*this)[i].n() );
  }
  electrodeCounter.erase( -1 );
  for ( set < int >::iterator it = electrodeCounter.begin(); it != electrodeCounter.end(); it ++ ){
    electrodePositions_.push_back( RealPos( (double)(*it), 0.0, 0.0 ) );
  }
}

int ElectrodeConfigVector::save( const string & fileName, const string & elecString, const string & formatString, bool fromOne ) const {

  fstream file; if ( !openOutFile( fileName, & file ) ) return 0;
  int prePrecision = file.precision();
  file.precision( 14 );

  file << electrodePositions_.size() << endl;

  vector < string > format( getSubstrings( inputFormatStringElecs_ ) );
  file << "# " << elecString << endl;

  for ( int i = 0, imax = electrodePositions_.size(); i < imax; i ++ ){
    for ( uint j = 0; j < format.size(); j ++ ){
      if ( format[ j ] == "x" || format[ j ] == "X" ) file << electrodePositions_[ i ].x();
      if ( format[ j ] == "y" || format[ j ] == "Y" ) file << electrodePositions_[ i ].y();
      if ( format[ j ] == "z" || format[ j ] == "Z" ) file << electrodePositions_[ i ].z();
      if ( format[ j ] == "d" || format[ j ] == "D" ) file << depths_[ i ];
      if ( j < format.size() -1 ) file << "\t";
    }
    file << endl;
  }

  int count = 0;
  for ( int i = 0, imax = this->size(); i < imax; i ++ ){
    if ( (*this)[i].isEnabled() ) count ++;
  }

  std::cout << "save:" << formatString << std::endl;
  file << count << endl;
  file << "# " << formatString << endl;

  format = getSubstrings( formatString );

  // format: a b m n rhoA K
  for ( int i = 0, imax = this->size(); i < imax; i ++ ){
    if ( !(*this)[i].isEnabled() ) continue;

    file.setf( ios::scientific, ios::floatfield );
    file.precision( 14 );

    for ( uint j = 0; j < format.size(); j ++ ){
      if ( format[ j ] == "a" || format[ j ] == "A" ||
	   format[ j ] == "c1" || format[ j ] == "C1" ) file << (*this)[ i ].a() + fromOne;
      else if ( format[ j ] == "b" || format[ j ] == "B" ||
		format[ j ] == "c2" || format[ j ] == "C2" )	file << (*this)[ i ].b() + fromOne;
      else if ( format[ j ] == "m" || format[ j ] == "M" ||
		format[ j ] == "p1" || format[ j ] == "P1" ) file << (*this)[ i ].m() + fromOne;
      else if ( format[ j ] == "n" || format[ j ] == "N" ||
		format[ j ] == "p2" || format[ j ] == "P2" ) file << (*this)[ i ].n() + fromOne;
      else if ( format[ j ] == "rhoa" || format[ j ] == "Rhoa" ||
		format[ j ] == "ra" || format[ j ] == "Ra" ) file << (*this)[ i ].rhoA();
      else if ( format[ j ] == "rho" || format[ j ] == "Rho" ||
		format[ j ] == "r" || format[ j ] == "R" ||
		format[ j ] == "r(Ohm)" || format[ j ] == "R(Ohm)" ||
		format[ j ] == "U/I" || format[ j ] == "u/i" ) file << (*this)[ i ].r();
      else if ( format[ j ] == "err" || format[ j ] == "Err" ) file << (*this)[ i ].err();
      else if ( format[ j ] == "err(%)" || format[ j ] == "Err(%)" ) file << (*this)[ i ].err() * 100.0;
      else if ( format[ j ] == "ip" || format[ j ] == "IP" ) file << (*this)[ i ].ip();
      else if ( format[ j ] == "iperr") file << (*this)[ i ].iperr();
      else if ( format[ j ] == "u" || format[ j ] == "U" ) file << (*this)[ i ].u();
      else if ( format[ j ] == "u(V)" || format[ j ] == "U(V)" ) file << (*this)[ i ].u();
      else if ( format[ j ] == "u(mV)" || format[ j ] == "U(mV)" ) file << (*this)[ i ].u() * 1000.0;
      else if ( format[ j ] == "u/mV" || format[ j ] == "U/mV" ) file << (*this)[ i ].u() * 1000.0;
      else if ( format[ j ] == "u[mV]" || format[ j ] == "U[mV]" ) file << (*this)[ i ].u() * 1000.0;
      else if ( format[ j ] == "i" || format[ j ] == "I" ) file << (*this)[ i ].i();
      else if ( format[ j ] == "i(A)" || format[ j ] == "I(A)" ) file << (*this)[ i ].i();
      else if ( format[ j ] == "i(mA)" || format[ j ] == "I(mA)" ) file << (*this)[ i ].i() * 1000.0;
      else if ( format[ j ] == "i/mA" || format[ j ] == "I/mA" ) file << (*this)[ i ].i() * 1000.0;
      else if ( format[ j ] == "i[mA]" || format[ j ] == "I[mA]" ) file << (*this)[ i ].i() * 1000.0;
      else if ( format[ j ] == "k" || format[ j ] == "K" )  file << (*this)[ i ].k();
      else if ( format[ j ] == "t" || format[ j ] == "T" )  file << (*this)[ i ].t();
      else if ( format[ j ] == "t(min)" || format[ j ] == "T(min)" )  file << (*this)[ i ].t() / 60.0;
      else {
	cerr << WHERE_AM_I << " format description not defined: format[ " << j << " ] = " << format[ j ] << endl;
      }
      if ( j < format.size() -1 ) file << "\t";
    }

    file << endl;
  }
  file.close();

return 1;
}

int ElectrodeConfigVector::readGeoTomFile( const string & fileName ){
  fstream file; openInFile( fileName, & file );
  electrodePositions_.clear();

  char strDummy[128];
  char c;
  char profilName[128];

  while ( file.get( c ) ){
    if ( c == '/' || c == ';' ) file.ignore(1000, '\n');
    else if ( isalpha( c ) ){
      file.unget();
      file >> profilName;
    }
    else if ( isdigit( c) ){
      file.unget();
      break;
    }
  }

  double spacing = 0.0; file >> spacing;
  int profilType = 0; file >> profilType;
  int nData = 0; file >> nData;
  int dummyInt = 0;
  file >> dummyInt;
  file >> dummyInt;

  cout << "Profilename = " << profilName << endl;
  cout << "Spacing = " << spacing << " nData = " << nData << endl;

  vector< double > xVals( nData ), dSpace( nData );
  vector< double > rhoA( nData ), current( nData ), deltaU( nData ), error( nData );
  vector< int > level( nData );
  char charDummy;
  for ( int i = 0; i < nData; i ++ ){
    switch( profilType ){
    case 1: // Wennner
      file >> xVals[ i ] >> dSpace[ i ] >> rhoA[ i ] >> charDummy;
      if ( charDummy != ';') {
	if ( charDummy != '!' ) file.unget();
	current[ i ] = 0.0;
	deltaU[ i ] = 0.0;
	error[ i ] = 0.0;
      } else {
	file >> current[ i ] >> deltaU[ i ] >> error[ i ];
      } break;
    case 3: // DipolDipol
      file >> xVals[ i ] >> dSpace[ i ] >> level[i] >> rhoA[ i ] >> charDummy;
      if ( charDummy != ';') {
	if ( charDummy != '!' ) file.unget();
	current[ i ] = 0.0;
	deltaU[ i ] = 0.0;
	error[ i ] = 0.0;
      } else {
	file >> current[ i ] >> deltaU[ i ] >> error[ i ];
      } break;
    case 7: // Schlumberger
      file >> xVals[ i ] >> dSpace[ i ] >> level[i] >> rhoA[ i ] >> charDummy;
      if ( charDummy != ';') {
	if ( charDummy != '!' ) file.unget();
	current[ i ] = 0.0;
	deltaU[ i ] = 0.0;
	error[ i ] = 0.0;
      } else {
	file >> current[ i ] >> deltaU[ i ] >> error[ i ];
      } break;
    }
  }
  set< double > xElecs;
  for ( size_t i = 0; i < xVals.size(); i ++ ) {
    xElecs.insert( xVals[ i ] );
  }
  int nElecs = uniqD( xVals ).size();

  double xMin = min( xVals );
  double xMax = max( xVals );

  if ( xElecs.size() != ( ( xMax - xMin ) / spacing ) + 1 ){
    cout << xMax << "\t" << xMin << "\t" << spacing << endl;
    cout << " found : " << xElecs.size()
	 << " expected: " << ( ( xMax - xMin ) / spacing ) +1 << endl;
    xElecs.clear();
    for ( size_t i = 0; i < ( ( xMax - xMin ) / spacing ) + 1; i ++ ) {
      xElecs.insert( xMin + i * spacing );
    }
  }

  map< double, int > electrodes;
  int counter = 0;

  for ( set< double >::iterator it = xElecs.begin(); it != xElecs.end(); it ++){
    electrodes[ (*it) ] = counter;
    counter ++;
    electrodePositions_.push_back( RealPos( (*it), 0.0, 0.0 ) );
  }

  for ( int i = 0; i < (dSpace[ 0 ] / spacing) * 3; i ++ ){
    electrodePositions_.push_back( RealPos( electrodePositions_.back().x() + spacing, 0.0, 0.0 ) );
  }

  switch( profilType ){
  case 1: cout << "read Wenner" << endl; break;
  case 3: cout << "read DipolDipol" << endl; break;
  case 7: cout << "read Schlumberger" << endl; break;
  default: cerr << WHERE_AM_I << " profilType undefined: " << profilType << endl;
  }

  int a = 0, b = 0, m = 0, n = 0;
  for ( int i = 0; i < nData; i ++ ){
    switch( profilType ){
    case 1:
      a = electrodes[ xVals[ i ] ];
      m = a + (int)(dSpace[ i ] / spacing);
      n = m + (int)(dSpace[ i ] / spacing);
      b = n + (int)(dSpace[ i ] / spacing);
      break;
    case 3:
      a = electrodes[ xVals[ i ] ];
      b = a + (int)(dSpace[ i ] / spacing);
      m = b + (int)(dSpace[ i ] / spacing) * level[ i ];
      n = m + (int)(dSpace[ i ] / spacing);
      break;
    case 7:
      a = electrodes[ xVals[ i ] ];
      m = a + level[ i ];
      n = m + (int)(dSpace[ i ] / spacing);
      b = n + level[ i ];
      break;
    }

    //    cout << a << " " << b << " " << m << " " << n << " " << level[ i ] << endl;

    if ( rhoA[ i ] != 0.0 ){
      this->push_back( ElectrodeConfig( a, b, m, n ) ) ;
      this->back().setU( deltaU[ i ] / 100.0 );
      this->back().setI( current[ i ] / 100.0 );
      this->back().setR( this->back().u() / this->back().i() );
      this->back().setRhoA( rhoA[ i ] );
      //      this->back().setRhoA( rhoA[ i ] );
    }
  }

  file.close();

  if ( electrodePositions_.size() == 0 ){
    createDummyElectrodes();
  }
  return 1;
}

int ElectrodeConfigVector::readELTKRNFile( const string & fileName, double radius, bool verbose ){
  fstream file; openInFile( fileName, & file );
  electrodePositions_.clear();

  vector < string > row;
  vector < int > vA, vM;
  vector < double > vRhoA;

  while( (row = getRowSubstrings( file ) ).size() > 0 ){
    switch( row.size() ){
    case 3:
      vA.push_back( (int)rint(toDouble( row[ 0 ] )) ); vM.push_back( (int)rint( toDouble( row[ 1 ] )) ); vRhoA.push_back( toDouble( row[ 2 ] ) );
      break;
    case 4:
      vA.push_back( (int)rint(toDouble( row[ 0 ] )) ); vM.push_back( (int)rint( toDouble( row[ 1 ] )) ); vRhoA.push_back( toDouble( row[ 2 ] ) );
      break;
    default:
      cerr << WHERE_AM_I << " dont know how to handle " << row.size() << " columns." << endl;
      exit(1);
    }
  }
  file.close();

  if ( vA.size() == vM.size() && vA.size() == vRhoA.size() ){
  } else {
    cerr << WHERE_AM_I << " there is something wrong " << vA.size() << "==" << vM.size() << "=="<< vRhoA.size() << endl;
    return 0;
  }

  int nElecs = uniqI( vA ).size();
  if ( verbose ) cout << "found "<< nElecs << " Electrodes and " << vRhoA.size() << " data." << endl;

  Domain2D circ; createCircle( circ, RealPos( 0.0, 0.0, 0.0 ), radius, radius, nElecs, -90.0, -1, 1, 1);
  for ( int i = 0; i < circ.nodeCount(); i ++ ) electrodePositions_.push_back( RealPos( circ.node( i ).pos() ) );

  int a = 0, b = 0, m = 0, n = 0;
  for ( size_t i = 0; i < vA.size(); i ++ ){
    a = vA[ i ];
    b = a + 1;
    if ( b > nElecs - 1 ) b -= nElecs;
    m = vM[ i ];
    n = m + 1;
    if ( n > nElecs - 1 ) n -= nElecs;

    this->push_back( ElectrodeConfig( a, b, m, n ) );
    this->back().setRhoA( vRhoA[ i ] );
  }

  return 1;
}

// int ElectrodeConfigVector::readGeoTomFileDipolDipol( fstream & file, double spacing, int nData ){
//   // A a B a M a N
//   cout << "read DipolDipol" << endl;

//   vector< double > xVals( nData ), dSpace( nData );
//   vector< double > rhoA( nData ), current( nData ), deltaU( nData ), error( nData );
//   vector< int > level( nData );
//   char charDummy;
//   for ( int i = 0; i < nData; i ++ ){
//     file >> xVals[ i ] >> dSpace[ i ] >> level[ i ] >> rhoA[ i ] >> charDummy;
//     file >> current[ i ] >> deltaU[ i ] >> error[ i ];
//   }

//   set< double > xElecs;
//   for ( size_t i = 0; i < xVals.size(); i ++ ) xElecs.insert( xVals[ i ] );
//   map< double, int > electrodes;
//   int counter = 0;
//   for ( set< double >::iterator it = xElecs.begin(); it != xElecs.end(); it ++){
//     electrodes[ (*it) ] = counter;
//     counter ++;
//   }

//   int a = 0, b = 0, m = 0, n = 0;
//   for ( int i = 0; i < nData; i ++ ){
//     this->push_back( ElectrodeConfig( a, b, m, n ) ) ;
//     this->back().setDeltaU( deltaU[ i ] / 100.0 );
//     this->back().setCurrent( current[ i ] / 100.0 );
//     this->back().setRhoA( rhoA[ i ] );
//     this->back().updateK();
//   }

//   return 1;
// }
// int ElectrodeConfigVector::readGeoTomFileSchlumberger( fstream & file, double spacing, int nData ){
//   cout << "read Schlumberger" << endl;
//   return 1;
// }

void ElectrodeConfigVector::collectPotentials( const DataMap & map ){
  RVector dU = map.data( *this );
  RVector dURez = map.data( *this, 1 );
  setU( dU );
  setURez( dURez );
}

void ElectrodeConfigVector::merge( ElectrodeConfigVector & profile ){
  TO_IMPL
}

RVector ElectrodeConfigVector::rhoA() const {
  RVector tmp( this->size() );
  for ( int i = 0, imax = tmp.size(); i < imax; i ++ ) tmp[ i ] = (*this)[ i ].rhoA();
  return tmp;
}
void ElectrodeConfigVector::setRhoA( const RVector & vec ){
  for ( int i = 0, imax = vec.size(); i < imax; i ++ ) (*this)[ i ].setRhoA( vec[ i ] );
}

RVector ElectrodeConfigVector::u() const{
  RVector tmp( this->size() );
  for ( int i = 0, imax = tmp.size(); i < imax; i ++ ) tmp[ i ] = (*this)[ i ].u();
  return tmp;
}
void ElectrodeConfigVector::setU( const RVector & vec ){
  for ( int i = 0, imax = vec.size(); i < imax; i ++ ) (*this)[ i ].setU( vec[ i ] );
}

RVector ElectrodeConfigVector::uRez() const{
  RVector tmp( this->size() );
  for ( int i = 0, imax = tmp.size(); i < imax; i ++ ) tmp[ i ] = (*this)[ i ].uRez();
  return tmp;
}
void ElectrodeConfigVector::setURez( const RVector & vec ){
  for ( int i = 0, imax = vec.size(); i < imax; i ++ ) (*this)[ i ].setURez( vec[ i ] );
}

RVector ElectrodeConfigVector::r() const{
  RVector tmp( this->size() );
  for ( int i = 0, imax = tmp.size(); i < imax; i ++ ) tmp[ i ] = (*this)[ i ].r();
  return tmp;
}
void ElectrodeConfigVector::setR( const RVector & vec ){
  for ( int i = 0, imax = vec.size(); i < imax; i ++ ) (*this)[ i ].setR( vec[ i ] );
}

RVector ElectrodeConfigVector::i() const{
  RVector tmp( this->size() );
  for ( int i = 0, imax = tmp.size(); i < imax; i ++ ) tmp[ i ] = (*this)[ i ].i();
  return tmp;
}
void ElectrodeConfigVector::setI( const RVector & vec ){
  for ( int i = 0, imax = vec.size(); i < imax; i ++ ) (*this)[ i ].setI( vec[ i ] );
}

RVector ElectrodeConfigVector::t() const {
  RVector tmp( this->size() );
  for ( int i = 0, imax = tmp.size(); i < imax; i ++ ) tmp[ i ] = (*this)[ i ].t();
  return tmp;
}
void ElectrodeConfigVector::setT( const RVector & vec ){
  for ( int i = 0, imax = vec.size(); i < imax; i ++ ) (*this)[ i ].setT( vec[ i ] );
}

RVector ElectrodeConfigVector::err() const{
  RVector tmp( this->size() );
  for ( int i = 0, imax = tmp.size(); i < imax; i ++ ) tmp[ i ] = (*this)[ i ].err();
  return tmp;
}
void ElectrodeConfigVector::setErr( const RVector & vec ){
  for ( int i = 0, imax = vec.size(); i < imax; i ++ ) (*this)[ i ].setErr( vec[ i ] );
}

RVector ElectrodeConfigVector::ip() const{
  RVector tmp( this->size() );
  for ( int i = 0, imax = tmp.size(); i < imax; i ++ ) tmp[ i ] = (*this)[ i ].ip();
  return tmp;
}
void ElectrodeConfigVector::setIp( const RVector & vec ){
  for ( int i = 0, imax = vec.size(); i < imax; i ++ ) (*this)[ i ].setIp( vec[ i ] );
}

RVector ElectrodeConfigVector::iperr() const{
  RVector tmp( this->size() );
  for ( int i = 0, imax = tmp.size(); i < imax; i ++ ) tmp[ i ] = (*this)[ i ].iperr();
  return tmp;
}
void ElectrodeConfigVector::setIperr( const RVector & vec ){
  for ( int i = 0, imax = vec.size(); i < imax; i ++ ) (*this)[ i ].setIperr( vec[ i ] );
}

RVector ElectrodeConfigVector::k() const{
  RVector tmp( this->size() );
  for ( int i = 0, imax = tmp.size(); i < imax; i ++ ) tmp[ i ] = (*this)[ i ].k();
  return tmp;
}
void ElectrodeConfigVector::setK( const RVector & vec ){
  for ( int i = 0, imax = vec.size(); i < imax; i ++ ) (*this)[ i ].setK( vec[ i ] );
}

void ElectrodeConfigVector::calcK( SpaceConfigEnum config, double mirrorPlaneZ ){
  for ( int i = 0, imax = this->size(); i < imax; i ++ ){
    (*this)[ i ].setK( 1.0 / standardisedPotential( (*this), i, config, mirrorPlaneZ ) );
  }
}

// void ElectrodeConfigVector::updateAllRhoA(){
//   for ( int i = 0, imax = this->size(); i < imax; i ++ ) (*this)[ i ].updateRhoA();
// }

// RVector ElectrodeConfigVector::allR() const {
//   RVector tmp( this->size() );
//   for ( int i = 0, imax = tmp.size(); i < imax; i ++ ){
//     tmp[ i ] = (*this)[ i ].r();
//   }
//   return tmp;
// }
// void ElectrodeConfigVector::setAllR( const RVector & allR ){
//   for ( int i = 0, imax = this->size(); i < imax; i ++ ) (*this)[ i ].setR( allR[ i ] );
// }

// RVector ElectrodeConfigVector::allT() const {
//   RVector tmp( this->size() );
//   for ( int i = 0, imax = tmp.size(); i < imax; i ++ ){
//     tmp[ i ] = (*this)[ i ].t();
//   }
//   return tmp;
// }
// void ElectrodeConfigVector::setAllT( const RVector & allT ){
//   for ( int i = 0, imax = allT.size(); i < imax; i ++ ){
//     (*this)[ i ].setT( allT[ i ] );
//   }
// }

// RVector ElectrodeConfigVector::allK() const {
//   RVector tmp( this->size() );
//   for ( int i = 0, imax = tmp.size(); i < imax; i ++ ){
//     tmp[ i ] = (*this)[ i ].k();
//   }
//   return tmp;
// }
// void ElectrodeConfigVector::setAllK( const RVector & allK ){
//   for ( int i = 0, imax =  allK.size(); i < imax; i ++ ){
//     (*this)[ i ].setK( allK[ i ] );
//   }
// }
// void ElectrodeConfigVector::updateAllK(){
//   for ( int i = 0, imax = this->size(); i < imax; i ++ ) (*this)[ i ].updateK();
// }

// RVector ElectrodeConfigVector::allDeltaU() const {
//   RVector tmp( this->size() );
//   for ( int i = 0, imax = tmp.size(); i < imax; i ++ ){
//     tmp[ i ] = (*this)[ i ].deltaU();
//   }
//   return tmp;
// }
// void ElectrodeConfigVector::setAllDeltaU( const RVector & allDeltaU ){
//   for ( int i = 0, imax = allDeltaU.size(); i < imax; i ++ ){
//     (*this)[ i ].setDeltaU( allDeltaU[ i ] );
//   }
// }
// RVector ElectrodeConfigVector::allDeltaURez() const {
//   RVector tmp( this->size() );
//   for ( int i = 0, imax = tmp.size(); i < imax; i ++ ){
//     tmp[ i ] = (*this)[ i ].deltaURez();
//   }
//   return tmp;
// }
// void ElectrodeConfigVector::setAllDeltaURez( const RVector & u ){
//   for ( int i = 0, imax = u.size(); i < imax; i ++ ){
//     (*this)[ i ].setDeltaURez( u[ i ] );
//   }
// }
// RVector ElectrodeConfigVector::allCurrent() const {
//   RVector tmp( this->size() );
//   for ( int i = 0, imax = tmp.size(); i < imax; i ++ ){
//     tmp[ i ] = (*this)[ i ].current();
//   }
//   return tmp;
// }
// void ElectrodeConfigVector::setAllCurrent( const RVector & u ){
//   for ( int i = 0, imax = u.size(); i < imax; i ++ ){
//     (*this)[ i ].setCurrent( u[ i ] );
//   }
// }

// void ElectrodeConfigVector::updateAllDeltaU(){
//   for ( int i = 0, imax = this->size(); i < imax; i ++ ) (*this)[ i ].updateDeltaU();
// }

// RVector ElectrodeConfigVector::allError() const {
//   RVector tmp( this->size() );
//   for ( int i = 0, imax = tmp.size(); i < imax; i ++ ){
//     tmp[ i ] = (*this)[ i ].error();
//   }
//   return tmp;
// }
// void ElectrodeConfigVector::setAllError( const RVector & allError ){
//  for ( int i = 0, imax = allError.size(); i < imax; i ++ ){
//     (*this)[ i ].setError( allError[ i ] );
//   }
// }

ElectrodeConfigSmartVector ElectrodeConfigSmartVector::filterElectIdx( const set < int > & idxSet ){
  vector < int > idxVec;
  for ( set < int >::iterator it = idxSet.begin(); it != idxSet.end(); it ++ ){
    idxVec.push_back( *it );
  }
  return filterElectIdx( idxVec );
}

ElectrodeConfigSmartVector ElectrodeConfigSmartVector::filterElectIdx( const vector < int > & idxVec ){
  ElectrodeConfigSmartVector newProfile;
//   cout << WHERE_AM_I << endl;
//   cout << "count " << electrodeCount() << " " << idxVec.size() << endl;

  for ( size_t i = 0; i < idxVec.size(); i ++ ){
    //    cout << i << " " << idxVec[ i ] -1 << endl;
    newProfile.createElectrode( *electrodes_[ idxVec[ i ] -1 ] );
  }

  //  newProfile.reorderElectrodes();

  map < int, Electrode * > eMap;
  for ( size_t i = 0; i < newProfile.electrodeCount(); i ++){
    //    cout << newProfile.electrode( i ).id() << endl;
    eMap[ newProfile.electrode( i ).id() ] = & newProfile.electrode( i );
  }

  Electrode *eA, *eB, *eM, *eN;
  for ( size_t i = 0; i < this->size(); i ++ ){
    eA = eMap[ (*this)[ i ]->eA()->id() ];
    eB = eMap[ (*this)[ i ]->eB()->id() ];
    eM = eMap[ (*this)[ i ]->eM()->id() ];
    eN = eMap[ (*this)[ i ]->eN()->id() ];
//     cout << (*this)[ i ]->eA()->id() << "\t"<< eA  << endl;
//     cout << (*this)[ i ]->eB()->id() << "\t"<< eB  << endl;
//     cout << (*this)[ i ]->eM()->id() << "\t"<< eM  << endl;
//     cout << (*this)[ i ]->eN()->id() << "\t" << eN  << endl;
    if ( eA != 0 && eB != 0 && eM != 0 && eN != 0){
      newProfile.push_back( new ElectrodeConfigSmart( *eA, *eB, *eM, *eN ) );
      newProfile.back()->copyAllVals( *(*this)[ i ] );
    }
  }
  //  cout << "newProfile: nElecs=" << newProfile.electrodeCount() << endl;

  newProfile.reorderElectrodes();
  return newProfile;
}

vector < ElectrodeConfigSmartVector > ElectrodeConfigSmartVector::profileLines( ){

  vector < set < int > > profilesElecsIdx;

  cout << "Found " << this->size() << " data and " << electrodes_.size() << " electrodes." << endl;

  if ( this->size() > 0 ){
    profilesElecsIdx.push_back( (*new set < int >) );
    profilesElecsIdx.back().insert( (*this)[ 0 ]->eA()->id() );
    profilesElecsIdx.back().insert( (*this)[ 0 ]->eB()->id() );
    profilesElecsIdx.back().insert( (*this)[ 0 ]->eM()->id() );
    profilesElecsIdx.back().insert( (*this)[ 0 ]->eN()->id() );

    bool profileFound = false;
    for ( size_t i = 1, imax = this->size(); i < imax; i ++ ){
      profileFound = false;

      for ( size_t j = 0, jmax = profilesElecsIdx.size(); j < jmax; j++ ){
	if ( (*this)[i]->containsIdx( profilesElecsIdx[ j ] ) ){
	  profilesElecsIdx[ j ].insert( (*this)[ i ]->eA()->id() );
	  profilesElecsIdx[ j ].insert( (*this)[ i ]->eB()->id() );
	  profilesElecsIdx[ j ].insert( (*this)[ i ]->eM()->id() );
	  profilesElecsIdx[ j ].insert( (*this)[ i ]->eN()->id() );
	  profileFound = true;
	  break;
	}
      }
      if ( !profileFound ) {
	profilesElecsIdx.push_back( *new set < int > );
	profilesElecsIdx.back().insert( (*this)[ i ]->eA()->id() );
	profilesElecsIdx.back().insert( (*this)[ i ]->eB()->id() );
	profilesElecsIdx.back().insert( (*this)[ i ]->eM()->id() );
	profilesElecsIdx.back().insert( (*this)[ i ]->eN()->id() );
      }
    }
  } else {
    cerr << WHERE_AM_I << " no data found " << endl;
  }
  cout << "Found " << profilesElecsIdx.size() << " profiles." << endl;
//   for ( set < int >::iterator it = profilesElecsIdx[0].begin(); it !=profilesElecsIdx[0].end(); it ++ ){
//     cout << (*it) << endl;
//   }

  vector < ElectrodeConfigSmartVector > profiles;

  size_t testSum = 0;
  for ( size_t i = 0; i < profilesElecsIdx.size(); i ++ ){
    profiles.push_back( filterElectIdx( profilesElecsIdx[ i ] ) );
    testSum += profiles.back().size();
  }

//   cout << profiles[0].electrodeCount() << endl;
//   profiles[0].save("tmp.profile");
//   exit(0);

  if ( testSum != this->size() ){
    if ( testSum < this->size() ){
      cerr << WHERE_AM_I << " nicht alle Daten zugeordnet " << testSum << "!=" << this->size() << endl;
    } else if ( testSum > this->size() ) {
      cerr << WHERE_AM_I << " some redundant profiles found " << testSum << "!=" << this->size() << endl;
    }

    for ( size_t i = 0; i < profiles.size(); i ++ ){
      cout << "Profile " << i << " E:" << profiles[i].electrodeCount() << " D: " << profiles[i].size() << endl;
    }
  }
    //    exit( 1 );

  return profiles;
}

void ElectrodeConfigSmartVector::renumberElectrodes(){
  for ( size_t i = 0; i < electrodes_.size(); i ++ ){
    electrodes_[ i ]->setId( i + 1 );
  }
}

void ElectrodeConfigSmartVector::reorderElectrodes(){
//     cerr << WHERE_AM_I << " failes" << endl;
//     return;
  //** sortiere und renumber
//   cout << WHERE_AM_I << endl;
//   cout << electrodeCount() << endl;

  map < double, Electrode *> distMap, spanMap;
  for ( size_t i = 0; i < electrodes_.size(); i ++ ){
    //    cout <<  electrodes_[ 0 ]->distance( *electrodes_[ i ] ) << endl;
    spanMap[ electrodes_[ 0 ]->distance( *electrodes_[ i ] ) ] = electrodes_[ i ];
  }

  if ( spanMap.size() != electrodeCount() ){
    cerr << WHERE_AM_I << " failes" << endl;
    return;
  }

  Electrode * eStart = NULL;
  for ( map < double, Electrode * >::iterator it= spanMap.begin(); it != spanMap.end(); it ++ ){
    eStart = it->second;
  }

  for ( size_t i = 0; i < electrodes_.size(); i ++ ){
    distMap[ eStart->distance( *electrodes_[ i ] ) ] = electrodes_[ i ];
  }

  vector < Electrode * > newElectrodes;
  //  cout << "dist " << distMap.size() << endl;

  int count = 0;
  for ( map < double, Electrode *>::iterator it=distMap.begin(); it != distMap.end(); it ++ ){
    newElectrodes.push_back( it->second );
    count++;
    newElectrodes.back( )->setId( count );
  }
  electrodes_ = newElectrodes;

  //  cout << electrodeCount() << endl;
  renumberElectrodes();

//   for ( int i = 0; i < newElectrodes.size(); i ++ ){
//     //    cout << electrodes_[i] << " " <<
//       newElectrodes[i]  << " " <<electrodes_[i]->id() << " " << newElectrodes[i]->id() << endl;
//   }
  //  cout << WHERE_AM_I << endl;
}

vector < RealPos > ElectrodeConfigSmartVector::electrodePositions( ){
  vector < RealPos > elecs;
  for ( size_t i = 0; i < electrodes_.size(); i ++ ){
    elecs.push_back( electrodes_[ i ]->pos() );
  }
  return elecs;
}

void ElectrodeConfigSmartVector::add( ElectrodeConfigSmart & data ){
  bool exists = false;

  Electrode *electrode[ 4 ];
  for ( size_t i = 0; i < 4; i ++ ){
    exists = false;
    for ( size_t j = 0; j < electrodeCount(); j ++ ){
      if ( data.electrode( i )->pos() == electrodes_[ j ]->pos() ) {
	electrode[ i ] = electrodes_[ j ];
	exists = true;
	break;
      }
    }
    if ( !exists ){
      electrodes_.push_back( new Electrode( *data.electrode( i ) ) );
      electrodes_.back()->setId( electrodes_.size() );
      electrode[ i ] = electrodes_.back();
    }
  }
//   for ( int i = 0; i < 4; i ++ ){
//     cout << electrode[ i ]->id() << electrode[ i ]->pos() << endl;
//   }
//   cout << endl;
  this->push_back( new ElectrodeConfigSmart( *electrode[ 0 ], *electrode[ 1 ],
					     *electrode[ 2 ], *electrode[ 3 ] ) );

  this->back()->copyAllVals( data );
}

void ElectrodeConfigSmartVector::add( ElectrodeConfigSmartVector & profile ){

  int exists = -1;
  for ( size_t i = 0; i < profile.electrodeCount(); i ++ ){
    exists = -1;
    for ( size_t j = 0; j < electrodeCount(); j ++ ){
      if ( profile.electrode( i ) == *electrodes_[ j ] ) {
	exists = electrodes_[ j ]->id();
	break;
      }
    }
    if ( exists != -1 ){
      profile.electrode( i ).setId( exists );
    } else {
      electrodes_.push_back( new Electrode( profile.electrode( i ) ) );
      electrodes_.back()->setId( electrodeCount() );
      profile.electrode( i ).setId( electrodeCount() );
    }
  }
  for ( size_t i = 0; i < profile.size(); i ++ ){
    this->push_back( new ElectrodeConfigSmart( *electrodes_[ profile[ i ]->eA()->id()-1 ],
					       *electrodes_[ profile[ i ]->eB()->id()-1 ],
					       *electrodes_[ profile[ i ]->eM()->id()-1 ],
					       *electrodes_[ profile[ i ]->eN()->id()-1 ] ) );

    this->back()->copyAllMember( (*profile[i]) );
  }
}

int ElectrodeConfigSmartVector::swapYZ( ){
  double tmpZ;
  for ( size_t i = 0; i < electrodes_.size( ); i ++ ){
    tmpZ = electrodes_[ i ]->z();
    electrodes_[ i ]->setZ( electrodes_[ i ]->y() );
    electrodes_[ i ]->setY( tmpZ );
  }
  return 1;
}
int ElectrodeConfigSmartVector::scale( const RealPos & start, const RealPos & end ){
  size_t eCount = electrodes_.size( );

  RealPos spacing( ( end - start) / (double)(eCount-1) );
  for ( size_t i = 0; i < eCount; i ++ ){
    electrodes_[ i ]->setPos( start + spacing * (double)i );
  }
  return 1;
}

int ElectrodeConfigSmartVector::translate( const RealPos & trans ){
  for ( size_t i = 0; i < electrodes_.size( ); i ++ ) electrodes_[ i ]->translate( trans );
  return 1;
}

int ElectrodeConfigSmartVector::load( const string & fileName, const string & formatString, bool fromOne, bool sortElecs ){
  ElectrodeConfigVector tmp;
  tmp.load( fileName, formatString, fromOne );

  inputFormatString_ = tmp.inputFormatString();

  this->clear();
  electrodes_.clear();

  for ( uint i = 0; i < tmp.electrodeCount(); i ++ ) createElectrode( tmp.electrodePos( i ), i + 1 );

  for ( uint i = 0; i < tmp.size(); i ++ ) {
    this->push_back( new ElectrodeConfigSmart( *electrodes_[ tmp[ i ].a() ],
					       *electrodes_[ tmp[ i ].b() ],
					       *electrodes_[ tmp[ i ].m() ],
					       *electrodes_[ tmp[ i ].n() ] ) );
    this->back()->copyAllVals( tmp[ i ] );
  }


  if ( sortElecs ){
    sort( (electrodes_.begin()), (electrodes_.end()), pPosX_less );
  }
  renumberElectrodes();

  return 1;
}

int ElectrodeConfigSmartVector::save( const string & fileName, const string & formatString, bool fromOne ){
  ElectrodeConfigVector tmp;

  for ( size_t i = 0, imax = electrodes_.size(); i < imax; i ++ ){
    tmp.addElectrode( RealPos( electrodes_[ i ]->x(), electrodes_[ i ]->y(), electrodes_[ i ]->z() ) );
  }

  for ( int i = 0, imax = this->size(); i < imax; i ++ ){
    tmp.push_back( ElectrodeConfig( (*this)[ i ]->eA()->id()- fromOne, (*this)[ i ]->eB()->id()- fromOne,
				    (*this)[ i ]->eM()->id()- fromOne, (*this)[ i ]->eN()->id()- fromOne ) );
    tmp.back().setRhoA( (*this)[ i ]->rhoA() );
    tmp.back().setK( (*this)[ i ]->k() );
    tmp.back().setU( (*this)[ i ]->u() );
    tmp.back().setI( (*this)[ i ]->i() );
    tmp.back().setR( (*this)[ i ]->r() );
    tmp.back().setT( (*this)[ i ]->t() );
    tmp.back().setErr( (*this)[ i ]->err() );
  }

  return tmp.save( fileName, formatString, fromOne );
}

void ElectrodeConfigSmartVector::setAllRhoA( const RVector & allRhoA ){
  for ( int i = 0, imax = allRhoA.size(); i < imax; i ++ ){
    (*this)[ i ]->setRhoA( allRhoA[ i ] );
  }
}

RVector ElectrodeConfigSmartVector::allRhoA() const {
    RVector tmp( this->size() );
    for ( int i = 0, imax = tmp.size(); i < imax; i ++ ){
        tmp[ i ] = (*this)[ i ]->rhoA();
    }
    return tmp;
}

int ElectrodeConfigSmartVector::saveEltomoFile( const string & fileName ){
  fstream file; if ( !openOutFile( fileName, &file ) ) return 0;
  file.setf( ios::fixed, ios::floatfield );
  file.precision( 14 );

  for ( size_t i = 0; i < this->size(); i ++ ){
    file << (*this)[ i ]->eA()->x() << "\t"
	 << (*this)[ i ]->eA()->y() << "\t"
	 << (*this)[ i ]->eA()->z() << "\t"
	 << (*this)[ i ]->eB()->x() << "\t"
	 << (*this)[ i ]->eB()->y() << "\t"
	 << (*this)[ i ]->eB()->z() << "\t"
	 << (*this)[ i ]->eM()->x() << "\t"
	 << (*this)[ i ]->eM()->y() << "\t"
	 << (*this)[ i ]->eM()->z() << "\t"
	 << (*this)[ i ]->eN()->x() << "\t"
	 << (*this)[ i ]->eN()->y() << "\t"
	 << (*this)[ i ]->eN()->z() << "\t"
      	 << (*this)[ i ]->rhoA() << "\t" ;
    if ( (*this)[ i ]->k() == 0.0 ){
      file << 1.0 / standardisedPotential( (*this).posA(i), (*this).posB(i),
					   (*this).posM(i), (*this).posN(i),
					   PLANE, 0.0 ) << endl;
    } else {
      file << (*this)[ i ]->k() << endl;
    }
  }

  file.close();

  return 1;
}

ostream & operator << ( ostream & str, ElectrodeConfigSmart & p ){
  str << p.eA()->id() << " " << p.eB()->id() << " " << p.eM()->id() << " " << p.eN()->id()
      <<  " : "  << p.rhoA() << endl;
  return str;
}

/*
$Log: electrodeconfig.cpp,v $
Revision 1.57  2009/05/20 12:30:09  carsten
add LOOPTETGEN option in invert script

Revision 1.56  2008/10/16 15:47:56  carsten
*** empty log message ***

Revision 1.55  2008/01/17 17:49:55  carsten
*** empty log message ***

Revision 1.54  2007/11/13 12:56:16  carsten
*** empty log message ***

Revision 1.53  2007/08/01 12:30:06  carsten
*** empty log message ***

Revision 1.52  2007/05/22 18:50:04  carsten
*** empty log message ***

Revision 1.51  2007/05/07 14:33:15  thomas
*** empty log message ***

Revision 1.50  2007/03/19 19:23:41  thomas
*** empty log message ***

Revision 1.49  2007/03/14 17:26:00  carsten
*** empty log message ***

Revision 1.48  2007/02/21 19:44:05  carsten
*** empty log message ***

Revision 1.47  2007/01/25 17:38:13  carsten
*** empty log message ***

Revision 1.46  2007/01/17 18:16:24  carsten
*** empty log message ***

Revision 1.45  2006/12/21 17:22:56  carsten
*** empty log message ***

Revision 1.44  2006/12/04 13:32:09  carsten
*** empty log message ***

Revision 1.43  2006/10/10 09:46:01  carsten
*** empty log message ***

Revision 1.41  2006/09/21 17:08:40  carsten
*** empty log message ***

Revision 1.40  2006/09/11 15:17:02  carsten
*** empty log message ***

Revision 1.39  2006/08/21 15:21:12  carsten
*** empty log message ***

Revision 1.38  2006/07/20 16:05:47  carsten
*** empty log message ***

Revision 1.37  2006/07/18 14:08:25  carsten
*** empty log message ***

Revision 1.36  2006/05/08 16:25:59  carsten
*** empty log message ***

Revision 1.35  2006/04/19 15:35:19  carsten
*** empty log message ***

Revision 1.34  2006/04/17 20:58:03  carsten
*** empty log message ***

Revision 1.33  2006/04/05 19:00:13  carsten
*** empty log message ***

Revision 1.32  2006/03/30 15:35:48  carsten
*** empty log message ***

Revision 1.31  2006/03/01 17:25:14  carsten
*** empty log message ***

Revision 1.30  2006/02/17 16:15:24  carsten
*** empty log message ***

Revision 1.29  2006/02/07 15:17:22  thomas
new constraint formulation - first steps

Revision 1.28  2006/01/29 17:54:05  carsten
*** empty log message ***

Revision 1.27  2006/01/26 18:24:06  carsten
*** empty log message ***

Revision 1.26  2005/10/14 15:31:37  carsten
*** empty log message ***

Revision 1.25  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.24  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.23  2005/07/20 14:04:48  carsten
*** empty log message ***

Revision 1.22  2005/07/13 17:05:21  carsten
*** empty log message ***

Revision 1.21  2005/07/13 12:27:39  carsten
*** empty log message ***

Revision 1.20  2005/07/06 13:15:20  carsten
*** empty log message ***

Revision 1.19  2005/06/30 17:32:16  carsten
*** empty log message ***

Revision 1.17  2005/06/27 09:28:55  carsten
*** empty log message ***

Revision 1.16  2005/06/23 20:15:32  carsten
*** empty log message ***

Revision 1.15  2005/05/31 10:50:04  carsten
*** empty log message ***

Revision 1.14  2005/04/15 13:11:20  carsten
*** empty log message ***

Revision 1.13  2005/04/12 11:53:54  carsten
*** empty log message ***

Revision 1.12  2005/04/12 11:41:09  carsten
*** empty log message ***

Revision 1.11  2005/04/11 13:12:35  carsten
*** empty log message ***

Revision 1.10  2005/03/20 22:51:59  carsten
*** empty log message ***

Revision 1.9  2005/03/18 19:47:17  carsten
*** empty log message ***

Revision 1.8  2005/03/17 18:17:13  carsten
*** empty log message ***

Revision 1.3  2005/01/13 21:12:15  carsten
*** empty log message ***

Revision 1.2  2005/01/06 14:16:14  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
