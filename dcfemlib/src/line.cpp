// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "line.h"
#include "solver.h"
#include "stlmatrix.h"

using namespace MyVec;

namespace MyMesh{ 
ostream & operator << ( ostream & str, const Line & l ){
  if ( l.valid() ){
    str << "Line: " << l.p0() << "-- " << l.p1() << " ";
  } else {
    str << "Line: invalid ";
  }
  return str;
}

Line::Line( ) {
  valid_ = false;
}

Line::Line( const RealPos & p ) 
: p1_( p ){
  p0_.setXYZ( 0.0, 0.0, 0.0 );
  valid_ = false;
  checkValidity();
}

Line::Line( const RealPos & p0, const RealPos & p1 )
  : p0_( p0 ), p1_( p1 ){
  valid_ = false;
  checkValidity();
}

Line::Line( const Edge & edge ){
  valid_ = false;
  p0_ = edge.nodeA().pos();
  p1_ = edge.nodeB().pos();
  checkValidity();
}

Line::Line( const Line & line ){
  valid_ = false;
  p0_ = line.p0();
  p1_ = line.p1();
  valid_ = line.valid();
}

Line::~Line(){
}

Line & Line::operator = ( const Line & line ){ 
  if ( this != & line ){
    p0_= line.p0();
    p1_= line.p1();
    valid_ = line.valid();
  } 
  return *this;
}      

bool Line::operator == ( const Line & line ){
  return compare( line );
}

bool Line::operator != ( const Line & line ){
  return !compare( line );
}

bool Line::checkValidity(){
  if ( p0_ != p1_ ) valid_ = true; else valid_ = false;
  return valid_; 
}

void Line::setPositions( const RealPos & p0, const RealPos & p1 ){
  p0_ = p0; p1_ = p1;
  checkValidity();
}

bool Line::compare( const Line & line, double epsilon ){
  if ( this->touch( line.p0() ) > 0 && this->touch( line.p1() ) > 0 ) return true;
  else return false;
}

RealPos Line::intersect( const Line & line) const {
  //** in 3D: wenn sie sich ber�hren spannen sie eine Ebene auf. 

// x0 + s * x1 = x2 + t * x3;					   
// x0 - x2 = t * x3 - s * x1;
						      
 MyVec::STLVector x0 = this->p0_.vec();								      
 MyVec::STLVector x1 = ( this->p1_ - this->p0_ ).vec();

 MyVec::STLVector x2 = line.p0().vec();							      
 MyVec::STLVector x3 = ( line.p1() - line.p0() ).vec();

 MyVec::STLVector b( x0 - x2 );
 MyVec::STLMatrix A( 3, 2 );
 A.setColumn( 0, -1.0 * x1 );
 A.setColumn( 1, x3 );
 MyVec::STLVector st( 2 );

 //** simple check if 2D
 if ( x1[2] == 0.0 && x3[2] == 0.0 ){
   MyVec::STLVector b2( 2 );
   b2[ 0 ] = b[ 0 ];
   b2[ 1 ] = b[ 1 ];
   MyVec::STLMatrix A2( 2, 2 );
   A2[ 0 ][ 0 ] = A[ 0 ][ 0 ]; A2[ 1 ][ 0 ] = A[ 1 ][ 0 ];
   A2[ 0 ][ 1 ] = A[ 0 ][ 1 ]; A2[ 1 ][ 1 ] = A[ 1 ][ 1 ];

   solveLU( A2, st, b2 );
//    cout << A2 << endl;
//    cout << b2[0] << " " << b2[1] << endl;

 } else {
   // L1 = P1 + a V1
   // L2 = P2 + b V2
   //   a (V1 X V2) = (P2 - P1) X V2

   RealPos P1( this->p0() );
   RealPos V1( this->p1() - this->p0() );
   RealPos P2( line.p0() );
   RealPos V2( line.p1() - line.p0() );

   double a = (P2-P1).cross( V2 ).abs() / V1.cross( V2 ).abs();

   if ( !isnan( a ) && !isnan( a ) ){
     return RealPos( P1 + V1 * a );
   }

   solveLU( A, st, b );
//     cout << A << endl;
//     cout << st[0] <<  " " << st[1] << endl;
//     cout << b[0] << " " << b[1] << " " << b[3] << endl;
 }

 double s = st[ 0 ];
 double t = st[ 1 ];

 if ( isnan( s ) || isinf( s ) || isnan( t ) || isinf( t ) ) {
   //   cout << WHERE_AM_I << " s: " << s << " t: " << t << endl;
   return RealPos();
 }

 RealPos iPos( x0 + x1 * s );

 if ( line.touch( iPos ) < 0 ){
   //cout << WHERE_AM_I << " touch: " << line.touch( iPos ) << endl;
   return RealPos();
 }

 return iPos; 
}

double Line::distance( const RealPos & pos ) const { 
  //**  http://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html;
  //** | x2-x1 cross x1-x0 | / | x2 -x1 |;
  //  cout << p1_ << p0_ << pos << endl;
  //cout << p1_ - p0_ << endl;
  //cout << (p1_ - p0_ ).cross( p0_ -pos ) << endl;
  //cout << "ret " << ( ( p1_ - p0_ ).cross( p0_ - pos ) ).abs() / ( p1_ - p0_ ).abs() << endl;
  return ( ( p1_ - p0_ ).cross( p0_ - pos ) ).abs() / ( p1_ - p0_ ).abs();
}

double Line::t( const RealPos & pos, double tol ) const {
  MyVec::STLVector t( RealPos( ( pos - p0_ ).round( tol ) / ( p1_ - p0_ ).round( tol ) ).vec() );
//   cout << tol << endl;
//   cout << ( pos - p0_ ).round( tol ) << ( p1_ - p0_ ).round( tol ) << endl;
//   cout << RealPos( ( pos - p0_ ) / ( p1_ - p0_ ) ) << endl;
//   cout << WHERE_AM_I << t << endl;

  if ( !isnan( t[ 0 ] ) && !isinf( t[ 0 ] ) ) return t[ 0 ];
  else if ( !isnan( t[ 1 ] ) && !isinf( t[ 1 ] ) ) return t[ 1 ];
  else if ( !isnan( t[ 2 ] ) && !isinf( t[ 2 ] ) ) return t[ 2 ];
  cerr << WHERE_AM_I << " pos is not at this line " << endl;
  return 0.0;
}

int Line::touch( const RealPos & pos, double tol, bool verbose ) const {

  if ( verbose ) cout << pos << tol << endl;
  if ( verbose ) cout << "Dist = " << fabs(this->distance( pos )) << endl;
  double length = p0_.distance( p1_ );
  double dist = fabs( this->distance( pos ) );
  if ( length > 1) tol*= length;
  if ( dist > ( tol ) * 10 ) {
    if ( verbose ) cout << "dist:" << dist << " length: " << length << " > " << "tol: " << ( tol ) * 10 << endl;
    return -1;
  }
  
  //  cout << "next" <<endl;
  if ( ( dist ) < tol ) dist = tol;
  double tsol = this->t( pos, dist );
  if ( verbose ) cout << p0_ << pos << p1_ << "Tsol: " << tsol << endl;

  if ( fabs( tsol ) < tol ) return 2;
  if ( tsol < 0 ) return 1;
  if ( fabs( 1 - tsol ) < tol ) return 4;
  if ( tsol > 1 ) return 5;

  return 3;
}
} // namespace MyMesh
/*
$Log: line.cpp,v $
Revision 1.5  2006/11/08 21:07:31  carsten
*** empty log message ***

Revision 1.4  2006/11/08 19:53:24  carsten
*** empty log message ***

Revision 1.3  2006/07/16 20:00:03  carsten
*** empty log message ***

Revision 1.2  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
