// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef INVERSIONSTUFF__H
#define INVERSIONSTUFF__H INVERSIONSTUFF__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "myvec/vector.h"
#include "myvec/matrix.h"
using namespace MyVec;

#include "basemesh.h"

DLLEXPORT double chiQuad( const RVector & soll, const RVector & ist, const RVector & err, bool isLog = true );
DLLEXPORT double linesearch( const RVector & dataVector, const RVector & oldRhoa, 
		   const RVector & newRhoa, const RVector & err, int iter = 0);

DLLEXPORT int savePotMatrix( const string & fname, const vector < RVector > & potMatrix );
DLLEXPORT int loadPotMatrix( const string & fname, vector < RVector > & potMatrix );


#endif // INVERSIONSTUFF__H

/*
$Log: inversionstuff.h,v $
Revision 1.6  2006/05/29 19:42:04  carsten
*** empty log message ***

Revision 1.5  2006/02/14 11:09:18  carsten
*** empty log message ***

Revision 1.4  2005/04/12 11:41:09  carsten
*** empty log message ***

Revision 1.3  2005/01/13 21:12:15  carsten
*** empty log message ***

Revision 1.2  2005/01/13 20:10:42  carsten
*** empty log message ***

Revision 1.1  2005/01/13 16:27:34  carsten
*** empty log message ***

*/
