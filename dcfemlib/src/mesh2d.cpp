// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//


#include "mesh2d.h"
#include "line.h"
#include "domain2d.h"
#include "refine.h"

#include "setalgorithm.h"
namespace MyMesh{

Mesh2D::Mesh2D() : BaseMesh( ){
}

Mesh2D::Mesh2D( const Mesh2D & mesh ) : BaseMesh( mesh ){
  for ( int i = 0; i < mesh.cellCount(); i++) BaseMesh::createCell( mesh.cell(i) );
  for ( int i = 0; i < mesh.boundaryCount(); i++) BaseMesh::createBoundary( mesh.boundary( i ) );
}

Mesh2D::Mesh2D( const string & fname ) : BaseMesh() {
  load( fname );
}
Mesh2D::Mesh2D( const Facet & facet ) : BaseMesh() {
  TO_IMPL
}

Mesh2D::~Mesh2D(){
 clear();
}

BaseElement * Mesh2D::createEdge_( Node & a, Node & b, int id, int marker ){
    Edge * e = findEdge( a, b );
    if ( !e ){
        if ( id == -1 ) id = pBoundaryVector_.size();
        e = new Edge( a, b, id, marker );
        pBoundaryVector_.push_back( e );
        for ( int i = 0; i < e->nodeCount(); i ++ ) e->node( i ).insertBoundary( e );
    }
    return e;
}

BaseElement * Mesh2D::createEdge3_( Node & a, Node & b, Node & c, int id, int marker ){
    Edge * e = findEdge( a, b );
    if ( !e ){
        if ( id == -1 ) id = pBoundaryVector_.size();
        e = new Edge3( a, b, c, id, marker );
        pBoundaryVector_.push_back( e );
        for ( int i = 0; i < e->nodeCount(); i ++ ) e->node( i ).insertBoundary( e );
    }
    return e;
}

BaseElement * Mesh2D::createTriangle_( Node & a, Node & b, Node & c, int id, double attribute ){
  if ( id == -1 ) id = pCellVector_.size();
  BaseElement * e = new Triangle( a, b, c, id, attribute );

  pCellVector_.push_back( e );
  for ( int i = 0; i < e->nodeCount(); i ++ ) e->node( i ).insertCell( e );
  return e;
}

BaseElement * Mesh2D::createTriangle6_( vector < Node* > pNodeVector, int id, double attribute ){
  if ( id == -1 ) id = pCellVector_.size();
  BaseElement * e = new Triangle6( *pNodeVector[ 0 ], *pNodeVector[ 1 ], *pNodeVector[ 2 ], id, attribute );
  for ( int i = 3; i < 6; i ++ ) e->setNode( i, *pNodeVector[ i ] );

  pCellVector_.push_back( e );
  for ( int i = 0; i < e->nodeCount(); i ++ ) e->node( i ).insertCell( e );
  return e;
}

BaseElement * Mesh2D::createCellFromNodeIdxVector( const vector < long > & idx, double attribute ){
  vector < Node * > pNodeVector;

  switch( idx.size() ){
  case 3: //** 3-node triangle
    return createTriangle_( node( idx[ 0 ] ), node( idx[ 1 ] ), node( idx[ 2 ] ), cellCount(), attribute ); break;
  case 6: //** 6-node triangle
    for ( int i = 0; i < 6; i ++ ) pNodeVector.push_back( &node( idx[ i ] ) );
    return createTriangle6_( pNodeVector, cellCount(), attribute );  break;
  default:
    cerr << WHERE_AM_I << " cell type undefined " << idx.size() << endl; exit( 1 );
  }
  return NULL;
}

BaseElement * Mesh2D::createBoundaryFromNodeIdxVector( const vector < long > & idx, int marker ){
  switch( idx.size() ){
  case 2:
    return createEdge_( node( idx[ 0 ] ), node( idx[ 1 ] ), boundaryCount(), marker ); break;
  case 3:
    return createEdge3_( node( idx[ 0 ] ), node( idx[ 1 ] ), node( idx[ 2 ] ), boundaryCount(), marker ); break;
  default:
    cerr << WHERE_AM_I << " boundary type undefined " << idx.size() << endl; exit( 1 );
  }
  return NULL;
}

void Mesh2D::merge( const BaseMesh & subMesh ){
  int oldNodeCount = nodeCount();
  for ( int i = 0; i < subMesh.nodeCount(); i ++ ){
    this->createNode( subMesh.node( i ).pos(), this->nodeCount(), 0 );
    //    cout << this->node( this->nodeCount() -1 ).id() << endl;
  }
  for ( int i = 0; i < subMesh.cellCount(); i ++ ) {
    this->createTriangle_( this->node( subMesh.cell( i ).node( 0 ).id() + oldNodeCount ),
			   this->node( subMesh.cell( i ).node( 1 ).id() + oldNodeCount ),
			   this->node( subMesh.cell( i ).node( 2 ).id() + oldNodeCount ),
			   this->cellCount(), subMesh.cell( i ).attribute() );
  }
}

BaseElement * Mesh2D::findBoundary( BaseElement & e0, BaseElement & e1 ) {
  SetpBaseElements edgesE0;
  edgesE0.insert( findEdge( e0.node( 0 ), e0.node( 1 ) ) );
  edgesE0.insert( findEdge( e0.node( 1 ), e0.node( 2 ) ) );
  edgesE0.insert( findEdge( e0.node( 2 ), e0.node( 0 ) ) );

  SetpBaseElements edgesE1;
  edgesE1.insert( findEdge( e1.node( 0 ), e1.node( 1 ) ) );
  edgesE1.insert( findEdge( e1.node( 1 ), e1.node( 2 ) ) );
  edgesE1.insert( findEdge( e1.node( 2 ), e1.node( 0 ) ) );

  SetpBaseElements commonEdges( edgesE0 & edgesE1 );

  //  cout << "common" << endl;
  //show( commonElements );

  if ( commonEdges.size() == 1 ) return (*commonEdges.begin());

  if ( edgesE0.size() > 0 ) show( edgesE0 );
  if ( edgesE1.size() > 0 ) show( edgesE1 );

  cerr << WHERE_AM_I << " commonEdges.size() " << commonEdges.size() << endl;

  return NULL;
}

Edge * Mesh2D::findEdge( Node & n0, Node & n1){
  return n0.findEdge( n1 );
}

void Mesh2D::qualityCheck(){
  TO_IMPL
}

void Mesh2D::createP2Mesh( const BaseMesh & mesh ){
  clear();

  for ( int i = 0, imax = mesh.nodeCount(); i < imax; i ++ ) {
      this->createNode( mesh.node( i ) );
  }

  SparcepNodeMatrix nodeMatrix( nodeCount() );
  vector < Node * > pNodeVector;

  Edge *edge = NULL;

  Node *n0 = NULL, *n1 = NULL, *n2 = NULL, *n3 = NULL, *n4 = NULL, *n5 = NULL;

  for ( int i = 0, imax = mesh.cellCount(); i < imax; i ++ ){
    pNodeVector.clear();
    n0 = &node( mesh.cell( i ).node( 0 ).id() );
    n1 = &node( mesh.cell( i ).node( 1 ).id() );
    n2 = &node( mesh.cell( i ).node( 2 ).id() );

    pNodeVector.push_back( n0 ); pNodeVector.push_back( n1 ); pNodeVector.push_back( n2 );

    if ( ( n3 = nodeMatrix.val( n0->id(), n1->id() ) ) == NULL ){
      n3 = createNode( ( n0->pos() + n1->pos() ) / 2.0, nodeCount(), markerT( n0, n1 ) );
      nodeMatrix.setVal( n0->id(), n1->id(), n3 );
    }
    if ( ( n4 = nodeMatrix.val( n1->id(), n2->id() ) ) == NULL ){
      n4 = createNode( ( n1->pos() + n2->pos() ) / 2.0, nodeCount(), markerT( n1, n2 ) );
      nodeMatrix.setVal( n1->id(), n2->id(), n4 );
    }
    if ( ( n5 = nodeMatrix.val( n2->id(), n0->id() ) ) == NULL ){
      n5 = createNode( ( n2->pos() + n0->pos() ) / 2.0, nodeCount(), markerT( n2, n0 ) );
      nodeMatrix.setVal( n2->id(), n0->id(), n5 );
    }
    pNodeVector.push_back( n3 ); pNodeVector.push_back( n4 ); pNodeVector.push_back( n5 );
    createTriangle6( pNodeVector, this->cellCount(), mesh.cell( i ).attribute() );

    edge = findEdge( mesh.cell( i ).node( 0 ), mesh.cell( i ).node( 1 ) );
    if ( edge != NULL ){
      if ( edge->marker() != 0 ) {
	createEdge3( *n0, *n1, *n3, this->boundaryCount(), edge->marker() );
      }
    }

    edge = findEdge( mesh.cell( i ).node( 1 ), mesh.cell( i ).node( 2 ) );
    if ( edge != NULL ){
      if ( edge->marker() != 0 ) {
	createEdge3( *n1, *n2, *n4, this->boundaryCount(), edge->marker() );
      }
    }

    edge = findEdge( mesh.cell( i ).node( 2 ), mesh.cell( i ).node( 0 ) );
    if ( edge != NULL ){
      if ( edge->marker() != 0 ) {
	createEdge3( *n2, *n0, *n5, this->boundaryCount(), edge->marker() );
      }
    }
  }
}

void Mesh2D::createH2Mesh( const BaseMesh & mesh ){

  vector < int > cellsToRefine;

  for ( int i = 0, imax = mesh.cellCount(); i < imax; i ++ ) cellsToRefine.push_back( i );
  createRefined( mesh, cellsToRefine );
}

int Mesh2D::createRefined( const BaseMesh & mesh, const vector < int > & cellIdx ){

  clear();

  // alle OrginalKnoten werden kopiert
  for ( int i = 0, imax = mesh.nodeCount(); i < imax; i ++ ) createNode( mesh.node( i ) );

  Node *n0 = NULL, *n1 = NULL, *n2 = NULL, *n3 = NULL, *n4 = NULL; Node *n5 = NULL;

  SparcepNodeMatrix nodeMatrix( nodeCount() );

  //** create nodes for all for refinement selected cell
  for ( int i = 0, imax = cellIdx.size(); i < imax; i ++ ){
    n0 = &node( mesh.cell( cellIdx[ i ] ).node( 0 ).id() );
    n1 = &node( mesh.cell( cellIdx[ i ] ).node( 1 ).id() );
    n2 = &node( mesh.cell( cellIdx[ i ] ).node( 2 ).id() );

    if ( ( n3 = nodeMatrix.val( n0->id(), n1->id() ) ) == NULL ){
      n3 = createNode( ( n0->pos() + n1->pos() ) / 2.0, nodeCount(), markerT( n0, n1 ) );
      nodeMatrix.setVal( n0->id(), n1->id(), n3 );
    }

    if ( ( n4 = nodeMatrix.val( n1->id(), n2->id() ) ) == NULL ){
      n4 = createNode( ( n1->pos() + n2->pos() ) / 2.0, nodeCount(), markerT( n1, n2 ) );
      nodeMatrix.setVal( n1->id(), n2->id(), n4 );
    }

    if ( ( n5 = nodeMatrix.val( n2->id(), n0->id() ) ) == NULL ){
      n5 = createNode( ( n2->pos() + n0->pos() ) / 2.0, nodeCount(), markerT( n2, n0 ) );
      nodeMatrix.setVal( n2->id(), n0->id(), n5 );
    }
  }

  Edge * edge = NULL;
  int marker = 0;
  for ( int i = 0; i < mesh.boundaryCount(); i++ ){
    edge = findEdge( mesh.boundary( i ).node( 0 ), mesh.boundary( i ).node( 1 ) );
    if ( edge != NULL ){
      n0 = &node( mesh.boundary( i ).node( 0 ).id() );
      n1 = &node( mesh.boundary( i ).node( 1 ).id() );
      n3 = nodeMatrix.val( n0->id(), n1->id() );

      marker = edge->marker();

      if ( n3 != NULL ){
	createEdge( *n0, *n3, boundaryCount(), marker );
	createEdge( *n3, *n1, boundaryCount(), marker );
      } else {
	createEdge( *n0, *n1, boundaryCount(), marker );
      }

      if ( marker != 0 ){
 	if ( n0->marker() != -99 ) n0->setMarker( marker );
 	if ( n1->marker() != -99 ) n1->setMarker( marker );
 	if ( n3 != NULL ) n3->setMarker( marker );
      }
    }
  }

  //** create all new cells
  for ( int i = 0, imax = mesh.cellCount(); i < imax; i ++ ){
    n0 = &node( mesh.cell( i ).node( 0 ).id() );
    n1 = &node( mesh.cell( i ).node( 1 ).id() );
    n2 = &node( mesh.cell( i ).node( 2 ).id() );

    n3 = nodeMatrix.val( n0->id(), n1->id() );
    n4 = nodeMatrix.val( n1->id(), n2->id() );
    n5 = nodeMatrix.val( n2->id(), n0->id() );

    if ( n3 == NULL && n4 == NULL && n5 == NULL ){
      createTriangle( *n0, *n1, *n2, mesh.cellCount(), mesh.cell( i ).attribute() );

    } else if ( n3 != NULL && n4 == NULL && n5 == NULL ) {
      createTriangle( *n3, *n2, *n0, mesh.cellCount(), mesh.cell( i ).attribute() );
      createTriangle( *n3, *n1, *n2, mesh.cellCount(), mesh.cell( i ).attribute() );
      createEdge( *n3, *n2, boundaryCount(), 0 );
    } else if ( n3 == NULL && n4 != NULL && n5 == NULL ) {
      createTriangle( *n4, *n2, *n0, mesh.cellCount(), mesh.cell( i ).attribute() );
      createTriangle( *n4, *n0, *n1, mesh.cellCount(), mesh.cell( i ).attribute() );
      createEdge( *n4, *n0, boundaryCount(), 0 );
    } else if ( n3 == NULL && n4 == NULL && n5 != NULL ) {
      createTriangle( *n5, *n1, *n2, mesh.cellCount(), mesh.cell( i ).attribute() );
      createTriangle( *n5, *n0, *n1, mesh.cellCount(), mesh.cell( i ).attribute() );
      createEdge( *n5, *n1, boundaryCount(), 0 );

    } else if ( n3 != NULL && n4 != NULL && n5 == NULL ) {
      createTriangle( *n3, *n1, *n4, mesh.cellCount(), mesh.cell( i ).attribute() );
      createEdge( *n3, *n4, boundaryCount(), 0 );
      if ( n3->distance( *n2 ) < n4->distance( *n0 ) ){
	createTriangle( *n3, *n4, *n2, mesh.cellCount(), mesh.cell( i ).attribute() );
	createTriangle( *n3, *n2, *n0, mesh.cellCount(), mesh.cell( i ).attribute() );
	createEdge( *n2, *n3, boundaryCount(), 0 );
      } else {
	createTriangle( *n4, *n0, *n3, mesh.cellCount(), mesh.cell( i ).attribute() );
	createTriangle( *n4, *n2, *n0, mesh.cellCount(), mesh.cell( i ).attribute() );
	createEdge( *n0, *n4, boundaryCount(), 0 );
      }

    } else if ( n3 == NULL && n4 != NULL && n5 != NULL ) {
      createTriangle( *n4, *n2, *n5, mesh.cellCount(), mesh.cell( i ).attribute() );
      createEdge( *n4, *n5, boundaryCount(), 0 );
      if ( n4->distance( *n0 ) < n5->distance( *n1 ) ){
	createTriangle( *n4, *n5, *n0, mesh.cellCount(), mesh.cell( i ).attribute() );
	createTriangle( *n4, *n0, *n1, mesh.cellCount(), mesh.cell( i ).attribute() );
	createEdge( *n0, *n4, boundaryCount(), 0 );
      } else {
	createTriangle( *n4, *n5, *n1, mesh.cellCount(), mesh.cell( i ).attribute() );
	createTriangle( *n5, *n0, *n1, mesh.cellCount(), mesh.cell( i ).attribute() );
	createEdge( *n1, *n5, boundaryCount(), 0 );
      }

    } else if ( n3 != NULL && n4 == NULL && n5 != NULL ) {
      createTriangle( *n5, *n0, *n3, mesh.cellCount(), mesh.cell( i ).attribute() );
      createEdge( *n3, *n5, boundaryCount(), 0 );
      if ( n3->distance( *n2 ) < n5->distance( *n1 ) ){
	createTriangle( *n5, *n3, *n2, mesh.cellCount(), mesh.cell( i ).attribute() );
	createTriangle( *n3, *n1, *n2, mesh.cellCount(), mesh.cell( i ).attribute() );
	createEdge( *n2, *n3, boundaryCount(), 0 );
      } else {
	createTriangle( *n5, *n3, *n1, mesh.cellCount(), mesh.cell( i ).attribute() );
	createTriangle( *n5, *n1, *n2, mesh.cellCount(), mesh.cell( i ).attribute() );
	createEdge( *n1, *n5, boundaryCount(), 0 );
      }

    } else if ( n3 != NULL && n4 != NULL && n5 != NULL ) {
      createTriangle( *n0, *n3, *n5, mesh.cellCount(), mesh.cell( i ).attribute() );
      createTriangle( *n1, *n4, *n3, mesh.cellCount(), mesh.cell( i ).attribute() );
      createTriangle( *n2, *n5, *n4, mesh.cellCount(), mesh.cell( i ).attribute() );
      createTriangle( *n3, *n4, *n5, mesh.cellCount(), mesh.cell( i ).attribute() );
      createEdge( *n3, *n4, boundaryCount(), 0 );
      createEdge( *n4, *n5, boundaryCount(), 0 );
      createEdge( *n5, *n3, boundaryCount(), 0 );
    }
  }

return 1;
}

int Mesh2D::refine(){
  FUTILE
  _edgeSubNode.resize( boundaryCount(), NULL);
  _elementRefineStatus.resize( cellCount(), 0);

//** Create Triangle StatusList 0 - no refine
//**                            1 - irregular refine
//**                            2 - regular refine (2er muster implementieren)
//**                            3 - regular refine
//**                            5 - finished;

  //      cout << WHERE_AM_I << endl;


  for ( VectorpElements::iterator it = refineList.begin(); it != refineList.end(); it++){
    divideElement( *(*it) );
  }

  //  cout << WHERE_AM_I << endl;

  int something_to_do = 1;
  while( something_to_do ){
    something_to_do = 0;
    for (unsigned int i = 0; i < _elementRefineStatus.size(); i ++ ){
      if (_elementRefineStatus[i] == 2 || _elementRefineStatus[i] == 3){
	divideElement( * pCellVector_[i] );
	something_to_do = 1;
      }
    }
  }

  //  cout << WHERE_AM_I << endl;

  for (unsigned int i = 0; i < _elementRefineStatus.size(); i ++ ){
    //cout << i << "\t" << _elementRefineStatus[i] << endl;
    if ( _elementRefineStatus[i] == 1 ){
      if (pCellVector_[i]->rtti() == MYMESH_TRIANGLE_RTTI)
	bisectTriangle( dynamic_cast< Triangle & >( *pCellVector_[i] ) );
    }
  }

  //        cout << WHERE_AM_I << endl;

	recount();
  refineList.clear();
  clearSubInfos();
  //  refineBadTriangles( 15 );
  return 1;
}

void Mesh2D::bisectTriangle(Triangle & triangle, Edge & edge, Node & newNode){
  FUTILE
//   int ready = 0;
//   Node * node0 = & triangle.node(0);
//   Node * node1 = & triangle.node(1);
//   Node * node2 = & triangle.node(2);
//   Node * oppositeNode;
//   Edge * newEdge;// = &edge;

//   //** find splited triangle.edge
//   //  cout << newNode.id()<< endl;
//   for ( int j = 0; j < 3; j ++ ){
//     //** find node which is not part of splited triangle.edge
//     if ( ( & triangle.node(j) != & edge.nodeA() ) &&
// 	 ( & triangle.node(j) != & edge.nodeB() ) ){
//       oppositeNode = & triangle.node(j);
//       //      cout << j << "\t" << oppositeNode->id() << endl;

//       if (oppositeNode->findEdge( newNode ) == NULL ){
// 	newEdge = createEdge( newNode, * oppositeNode, boundaryCount(), 0);
// 	switch(j){
// 	case 0:
// 	  triangle.setNodes( newNode, * oppositeNode, * node2 );
// 	  createTriangle( newNode, * oppositeNode, * node1, cellCount(), triangle.attribute() );
// 	  break;
// 	case 1:
// 	  triangle.setNodes( newNode, * oppositeNode, * node0 );
// 	  createTriangle( newNode, * oppositeNode, * node2, cellCount(), triangle.attribute() );
// 	  break;
// 	case 2:
// 	  triangle.setNodes( newNode, * oppositeNode, * node1 );
// 	  createTriangle( newNode, * oppositeNode, * node0, cellCount(), triangle.attribute() );
// 	  break;
// 	}
// 	ready = 1;
//       }
//     }
//     if (ready) break;
//   }
  //pCellVector_.back()->setRefineStatus( IRREGULAR );
// dynamic_cast< Triangle * >(pCellVector_.back())->setIrregEdge( * newEdge );
//   triangle.setRefineStatus( IRREGULAR );
//   triangle.setIrregEdge( *newEdge );
//  _elementRefineStatus[ triangle.id() ] = 5;

}

void Mesh2D::bisectTriangle(Triangle & triangle){
  FUTILE
//   int ready = 0;
//   Node * node0 = & triangle.node(0);
//   Node * node1 = & triangle.node(1);
//   Node * node2 = & triangle.node(2);
//   Node * newNode;
//   Node * oppositeNode;
//   Edge * newEdge;

//   for ( int i = 0; i < 3; i ++ ){
//     //** find splited triangle.edge
//     //    cout << i << "\t" <<  endl;
//     if ( _edgeSubNode[ triangle.edge(i).id() ] != NULL ){
//       newNode = _edgeSubNode[ triangle.edge(i).id() ];
//       //      cout << i << "\t" << newNode->id() << endl;
//       for ( int j = 0; j < 3; j ++ ){
// 	//** find node which is not part of splited triangle.edge
// 	if ( ( & triangle.node(j) != & triangle.edge(i).nodeA() ) &&
// 	     ( & triangle.node(j) != & triangle.edge(i).nodeB() ) ){
// 	  oppositeNode = & triangle.node(j);

// 	  //	  cout << j << "\t" << oppositeNode->id() << endl;

// 	  if ( oppositeNode->findEdge( * newNode ) == NULL ){
// 	    newEdge = createEdge( * newNode, * oppositeNode, boundaryCount(), 0);
// 	    switch(j){
// 	    case 0:
// 	      triangle.setNodes( * newNode, * oppositeNode, * node2 );
// 	      createTriangle( * newNode, * oppositeNode, * node1, cellCount(), triangle.attribute() );
// 	      break;
// 	    case 1:
// 	      triangle.setNodes( * newNode, * oppositeNode, * node0 );
// 	      createTriangle( * newNode, * oppositeNode, * node2, cellCount(), triangle.attribute() );
// 	      break;
// 	    case 2:
// 	      triangle.setNodes( * newNode, * oppositeNode, * node1 );
// 	      createTriangle( * newNode, * oppositeNode, * node0, cellCount(), triangle.attribute() );
// 	      break;
// 	    }
// 	    ready = 1;
// 	  }
// 	}
//       }
//     }
//     if (ready) break;
//   }
// //   pCellVector_.back()->setRefineStatus( IRREGULAR );
// // dynamic_cast< Triangle * >(pCellVector_.back())->setIrregEdge( * newEdge );
// //   triangle.setRefineStatus( IRREGULAR );
// //   triangle.setIrregEdge( *newEdge );
//   _elementRefineStatus.resize( cellCount(), 0);
//   _elementRefineStatus[ triangle.id() ] = 5;

}

void Mesh2D::improveMeshQuality( bool nodeMoving, bool edgeSwapping, int smoothFunktion, int smoothIteration ){
  //   //*** 1 upto 4 seems ok
  //  cout << "Warning ! nodeMoving only for convex Areas." << endl;

//   cout << WHERE_AM_I << " smoothing: "
//        << nodeMoving << " " << edgeSwapping << " " << smoothFunktion << " " << smoothIteration << endl;

  for ( int j = 0; j < smoothIteration; j++ ){
    if ( edgeSwapping ) {
      for (int i = 0; i < boundaryCount(); i++) dynamic_cast< Edge & >( boundary( i ) ).swap( 1 );
    }
    if ( nodeMoving ) {
      for (int i = 0; i < nodeCount(); i++) {
          if ( node(i).marker() == 0 ) node( i ).smooth( smoothFunktion );
      }
    }
  }
  recount();
}

void Mesh2D::divideElement( BaseElement & element ){
  FUTILE
  switch (element.rtti() ){
  case MYMESH_TRIANGLE_RTTI: divideTriangle( dynamic_cast< Triangle & >( element ) ); break;
  case MYMESH_QUADRANGLE_RTTI: divideQuadrangle( dynamic_cast< Quadrangle & >( element ) ); break;
  default : break;
  }

}

Edge & Mesh2D::splitEdge( Node & node, Edge & edge ){
  TO_IMPL;
  Edge * newedge = NULL;

//   if ( ( & edge.nodeA() != & node) && ( & edge.nodeB() != & node) ) {
//     Node * nodea = & edge.nodeA();
//     Node * nodeb = & edge.nodeB();

//     edge.setNodes( * nodea, node );
//     newedge = createEdge( node, * nodeb );

//     set< Edge * > edgeSet( node.fromSet() + node.toSet() );
//     for (set<Edge *>::iterator it = edgeSet.begin(); it !=edgeSet.end(); it ++){
//       if ( (*it) != & edge && *(*it) == edge ) {
//  	deleteEdge( edge );
// 	break;
//       }
//     }

//     for (set<Edge *>::iterator it = edgeSet.begin(); it !=edgeSet.end(); it ++){
//       if ( (*it) != newedge && *(*it) == (*newedge) ) {
// 	deleteEdge( * newedge );
// 	break;
//       }
//     }
//     newedge->setMarker( edge.marker() );
//     newedge->setId( boundaryCount() - 1 );

// //     cout << "Split edge new Edge = " << newedge->id()
// // 	 << " nodes: "<< newedge->nodeA().id() << "\t" <<newedge->nodeB().id() << endl;
//   }
  return * newedge;
}


void Mesh2D::divideQuadrangle( Quadrangle & quadrangle ){
  FUTILE
//   Node * node0 = & quadrangle.node(0);
//   Node * node1 = & quadrangle.node(1);
//   Node * node2 = & quadrangle.node(2);
//   Node * node3 = & quadrangle.node(3);

//   Edge * edge0 = & quadrangle.edge(0);
//   Edge * edge1 = & quadrangle.edge(1);
//   Edge * edge2 = & quadrangle.edge(2);
//   Edge * edge3 = & quadrangle.edge(3);

//   RealPos mid_pos; //** middle position on each edge;

//   //** split all edges and create 4 new nodes;
//   for ( int i = 0; i < 4; i ++ ){
//     if ( _edgeSubNode[ quadrangle.edge(i).id() ]  == NULL){
//       mid_pos = quadrangle.edge(i).nodeA().middlePosition(quadrangle.edge(i).nodeB() );
//       _edgeSubNode[ quadrangle.edge(i).id() ] = createNode(mid_pos.x(), mid_pos.y()
// 							, nodeCount(), quadrangle.edge(i).marker() );
//       splitEdge( * _edgeSubNode[ quadrangle.edge(i).id() ], quadrangle.edge(i) );

//       if ( ( quadrangle.edge(i).leftCell() == & quadrangle ) &&
// 	   ( quadrangle.edge(i).rightCell() != NULL) ){
// 	_elementRefineStatus[ quadrangle.edge(i).rightCell()->id() ] ++;

//       } else if ( ( quadrangle.edge(i).rightCell() == & quadrangle ) &&
// 		  ( quadrangle.edge(i).leftCell() != NULL) ) {
// 	_elementRefineStatus[ quadrangle.edge(i).leftCell()->id() ] ++;
//       }
//     }
//   }

//   //** create new Node in the Middle of each Quadrangle
//   mid_pos = quadrangle.middlePosition();
//   Node * middle = createNode( mid_pos.x(), mid_pos.y(), nodeCount(), 0);

//   createEdge( * _edgeSubNode[ edge0->id() ], * middle, boundaryCount(), 0 );
//   createEdge( * _edgeSubNode[ edge1->id() ], * middle, boundaryCount(), 0 );
//   createEdge( * _edgeSubNode[ edge2->id() ], * middle, boundaryCount(), 0 );
//   createEdge( * _edgeSubNode[ edge3->id() ], * middle, boundaryCount(), 0 );

//   //** change Nodes of origin Quadrangle
//   quadrangle.setNodes( * node0, * _edgeSubNode[ edge0->id() ],
// 		       * middle, * _edgeSubNode[ edge3->id() ]);

//   //** create 3 new Quadrangles

//   createQuadrangle( * _edgeSubNode[ edge0->id() ], * node1,
// 		 * _edgeSubNode[ edge1->id() ], * middle, cellCount(), quadrangle.attribute() );
//   createQuadrangle( * _edgeSubNode[ edge1->id() ], * node2,
// 		 * _edgeSubNode[ edge2->id() ], * middle, cellCount(), quadrangle.attribute() );
//   createQuadrangle( * _edgeSubNode[ edge2->id() ], * node3,
// 		 * _edgeSubNode[ edge3->id() ], * middle, cellCount(), quadrangle.attribute() );
//   _elementRefineStatus[ quadrangle.id() ] = 5;


}

void Mesh2D::divideTriangle( Triangle & triangle ){
  FUTILE
  //** if in past was triangle irregular refined
  if ( !1 ){
    //  if (triangle.refineStatus() == IRREGULAR){
    divideIregTriangle( triangle );
  } else {

    Node * node0 = & triangle.node(0);
    Node * node1 = & triangle.node(1);
    Node * node2 = & triangle.node(2);

    Edge * edge0 = & triangle.edge(0);
    Edge * edge1 = & triangle.edge(1);
    Edge * edge2 = & triangle.edge(2);

    //** create 3 new nodes if not exists
    RealPos mid_pos;
    for ( int i = 0; i < 3; i ++ ){
      //** check if node exist
      if ( _edgeSubNode[ triangle.edge(i).id() ]  == NULL){ //* if not
	mid_pos = triangle.edge(i).nodeA().middlePosition( triangle.edge(i).nodeB() );
	_edgeSubNode[ triangle.edge(i).id() ] = createNode(mid_pos.x(), mid_pos.y(),
							   nodeCount(), triangle.edge(i).marker() );
	splitEdge( * _edgeSubNode[ triangle.edge(i).id() ], triangle.edge(i) );

	if ( ( triangle.edge(i).leftCell() == & triangle ) &&
	     ( triangle.edge(i).rightCell() != NULL) ){
	  _elementRefineStatus[ triangle.edge(i).rightCell()->id() ] ++;

	} else if ( ( triangle.edge(i).rightCell() == & triangle ) &&
		    ( triangle.edge(i).leftCell() != NULL) ) {
	  _elementRefineStatus[ triangle.edge(i).leftCell()->id() ] ++;
	}
      }
    }

    createEdge( * _edgeSubNode[ edge0->id() ], * _edgeSubNode[ edge1->id() ], boundaryCount(), 0 );
    createEdge( * _edgeSubNode[ edge1->id() ], * _edgeSubNode[ edge2->id() ], boundaryCount(), 0 );
    createEdge( * _edgeSubNode[ edge2->id() ], * _edgeSubNode[ edge0->id() ], boundaryCount(), 0 );

    triangle.setNodes( * node0, * _edgeSubNode[ edge2->id() ], * _edgeSubNode[ edge0->id() ] );
    triangle.setRefineStatus( REGULAR );

    createTriangle( * _edgeSubNode[ edge0->id() ], * _edgeSubNode[ edge1->id() ],
		    * _edgeSubNode[ edge2->id() ], cellCount(), triangle.attribute() );
    pCellVector_.back()->setRefineStatus( REGULAR );

    createTriangle( * _edgeSubNode[ edge0->id() ], * node1, * _edgeSubNode[ edge1->id() ],
		    cellCount(), triangle.attribute() );
    pCellVector_.back()->setRefineStatus( REGULAR );

    createTriangle( * _edgeSubNode[ edge1->id() ], * node2, * _edgeSubNode[ edge2->id() ],
		    cellCount(), triangle.attribute() );
    pCellVector_.back()->setRefineStatus( REGULAR );

    _elementRefineStatus[ triangle.id() ] = 5;
  }

}

void Mesh2D::divideIregTriangle( Triangle & triangle ){
  FUTILE

  _elementRefineStatus[ triangle.id() ] = 5;

  Node * node0 = & triangle.node(0);
  Node * node1 = & triangle.node(1);
  Node * node2 = & triangle.node(2);

  Edge * edge0 = & triangle.edge(0);
  Edge * edge1 = & triangle.edge(1);
  Edge * edge2 = & triangle.edge(2);

  //  IREdge = & triangle.irregEdge();


}

void Mesh2D::refineBadTriangles( double angle ){
  FUTILE
  //** refine all Triangle with an lower angle
  double alpha, beta, gamma;

  vector< Triangle * > tl;

  for (int i = 0; i < cellCount(); i++){

    alpha = element(i).node(0).angle( element(i).node(1), element(i).node(2) );
    beta = element(i).node(1).angle( element(i).node(2), element(i).node(0) );
    gamma = PI_ - ( alpha + beta );

    if ( (alpha < (PI_ * (angle/180.) ) ) || (beta  < (PI_ * (angle/180.) ) ) ||  (gamma < (PI_ * (angle/180.) ) ) ) {
      tl.push_back( dynamic_cast<Triangle *>( &element(i) ) );
    }
  }

//   _edgeSubNode.resize( boundaryCount(), NULL);
//  _elementRefineStatus.resize( cellCount(), 0);

  for (vector <Triangle *>::iterator it = tl.begin(); it != tl.end(); it ++){
    //    if (_elementRefineStatus[ (*it)->id() ] == 0){
      refineBadTriangle( *(*it) );
      //    }
  }

}

void Mesh2D::refineBadTriangle( Triangle & triangle ){

  //** ganze funktion Q&D
  //** das ausgewaehlte Dreieck bekommt in der Mitte einen neuen Punkt
  //** ist eine Seite des dreiecks eine Interfacelinie( bissher nur aeussere)
  //** und ist der ihr gegenueberliegende Winkel stumpf dann bisection des Dreiecks
  Node * node0 = & triangle.node(0);
  Node * node1 = & triangle.node(1);
  Node * node2 = & triangle.node(2);
  Node * newNode, * nodea, * nodeb;
  Edge * edge;

  double angle[ 3 ];
  angle[ 0 ] = node0->angle( * node1, * node2 );
  angle[ 1 ] = node1->angle( * node2, * node0 );
  angle[ 2 ] = PI_ - ( angle[ 0 ] + angle[ 1 ] );
  double x, y, anglecheck;

  for ( int i = 0, imax = 3; i < imax; i++ ){
    //** if one edge an interfaceline
    edge = & triangle.edge( i );
    if ( i == 0 ) anglecheck = angle[ 2 ]; else anglecheck = angle[ i - 1 ];

    if ( ( edge->marker() != 0 ) && ( anglecheck > PI_/2. ) ){
      //      cout << "case 1 i = 0: " << edge->id() << endl;

      //** bisect Triangle at outher interface
      nodea = & triangle.node( i );
      if ( i == 2 ) nodeb = & triangle.node( 0 ); else nodeb = & triangle.node( i + 1 );

      x = nodea->middlePosition( * nodeb ).x();
      y = nodea->middlePosition( * nodeb ).y();

      newNode = createNode( x, y, nodeCount(), edge->marker() );
      //      cout << "split" << endl;
      splitEdge( * newNode, * edge );
      //      cout << "Bisectstart" << endl;
      bisectTriangle( triangle, * edge, * newNode );

      //** if inner interface bisect neighbourelement
      if (edge->marker() > 0 ) {
	if ( dynamic_cast< Triangle * >( edge->leftCell() ) != & triangle ){
	  bisectTriangle( * dynamic_cast< Triangle *>( edge->leftCell() ), * edge, * newNode );
	} else {
	  bisectTriangle( * dynamic_cast< Triangle *>( edge->rightCell() ), * edge, * newNode );
	}
      }
      return;
    }
  }
  //  cout << "case 2 " << endl;
   //** insert new Node in the middle of the Triangle
  x = ( node0->x() + node1->x() + node2->x() ) / 3.;
  y = ( node0->y() + node1->y() + node2->y() ) / 3.;

  newNode = createNode( x, y, nodeCount(), 0 );
  createEdge( * newNode, * node0, boundaryCount(), 0 );
  createEdge( * newNode, * node1, boundaryCount(), 0 );
  createEdge( * newNode, * node2, boundaryCount(), 0 );

  triangle.setNodes( * node0, * node1, * newNode );
  createTriangle( * node1, * node2, * newNode, cellCount(), triangle.attribute() );
  createTriangle( * node2, * node0, * newNode, cellCount(), triangle.attribute() );
  //  _elementRefineStatus.resize( cellCount(), 0);
  //  cout << "case 2 finish" << endl;
}

Polygon Mesh2D::findAllNodesBetween( Node * pNodeStart, Node * pNodeEnd, int marker ){
  map < double, Node * > nodeMap;

  nodeMap[ 0.0 ] = pNodeStart;
  nodeMap[ pNodeStart->distanceTo( *pNodeEnd ) ] = pNodeEnd;

  Line line( pNodeStart->pos(), pNodeEnd->pos() );

  for ( int i = 0, imax = this->nodeCount(); i < imax; i ++){
    if ( marker == -1 ){
      if ( line.touch( this->node( i ).pos() ) == 3 ){
	nodeMap[ pNodeStart->distanceTo( this->node( i ) ) ] = &this->node( i );
      }
    } else if ( this->node( i ).marker() == marker ){
      nodeMap[ pNodeStart->distanceTo( this->node( i ) ) ] = &this->node( i );
    }
  }

  Polygon poly;
  for ( map< double, Node * >::iterator it = nodeMap.begin(); it != nodeMap.end(); it ++){
    poly.push_back( it->second );
  }
  poly.setMarker( marker );
  return poly;
}


Plane Mesh2D::plane() const {

  if ( this->nodeCount() > 2 ){

    Line line( node( 0 ).pos(), node( 1 ).pos() );

    for ( int i = 0; i < this->nodeCount(); i ++ ){
      if ( line.distance( node( i ).pos() ) > TOLERANCE * 1e5 ){
	return Plane( node( 0 ).pos(), node( 1 ).pos(), node( i ).pos() );
      }
    }
  }
  //  cerr << WHERE_AM_I << " no plane found for this facet." << endl;
  return Plane();
}

int Mesh2D::getZ( const Domain2D & domain ){
  if ( this->nodeCount() >= domain.nodeCount() ){
    for ( int i = 0; i < domain.nodeCount(); i ++ ) {
      this->node( i ).setZ( domain.node( i ).z() );
    }
    Domain2D tmpDomain( domain );
    for ( int i = 0; i < tmpDomain.nodeCount(); i ++ ) tmpDomain.node( i ).setZ( 0.0 );
    int touchindex = -1;

    for ( int i = domain.nodeCount(); i < this->nodeCount(); i ++ ) {
      for ( int j = 0; j < tmpDomain.polygonCount(); j ++ ){
	if ( ( touchindex = tmpDomain.polygon( j ).touch( this->node( i ).pos() ) ) != -1 ){

	  double touchat = tmpDomain.polygon( j )[ touchindex ]->pos().distance( this->node( i ).pos() ) /
	    tmpDomain.polygon( j )[ touchindex ]->pos().distance( tmpDomain.polygon( j )[ touchindex +1]->pos() );
	  this->node( i ).setPos( Line( domain.polygon( j )[ touchindex ]->pos(),
					domain.polygon( j )[ touchindex + 1 ]->pos() ).lineAt( touchat ) );
	  break;
	}
      }
      if ( touchindex == -1 ){
	TO_IMPL;
      }
    }
  } else {
    cerr << WHERE_AM_I << " it seems there is no affiliation beetween the mesh and the domain" <<
      this->nodeCount() << " < " << domain.nodeCount() << endl;
    return -1;
  }
 return 1;
}



} // namespace MyMesh2D

/*
$Log: mesh2d.cpp,v $
Revision 1.34  2011/08/30 14:09:51  carsten
fix memory problem within mesh-copy

Revision 1.33  2010/10/26 12:20:28  carsten
win32 compatibility commit

Revision 1.32  2009/04/02 16:33:15  carsten
*** empty log message ***

Revision 1.31  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.30  2007/02/05 16:34:10  carsten
*** empty log message ***

Revision 1.29  2006/06/06 09:41:49  carsten
*** empty log message ***

Revision 1.28  2006/04/17 20:58:03  carsten
*** empty log message ***

Revision 1.27  2006/02/06 17:30:42  carsten
*** empty log message ***

Revision 1.26  2005/10/28 16:09:05  carsten
*** empty log message ***

Revision 1.25  2005/10/24 09:52:08  carsten
*** empty log message ***

Revision 1.24  2005/10/20 16:25:22  carsten
#

Revision 1.23  2005/10/18 16:29:56  carsten
*** empty log message ***

Revision 1.22  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.21  2005/10/12 14:41:56  carsten
*** empty log message ***

Revision 1.20  2005/10/12 12:35:21  thomas
*** empty log message ***

Revision 1.19  2005/10/09 18:54:44  carsten
*** empty log message ***

Revision 1.18  2005/08/30 15:43:28  carsten
*** empty log message ***

Revision 1.17  2005/08/12 16:43:20  carsten
*** empty log message ***

Revision 1.16  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.15  2005/07/13 17:05:21  carsten
*** empty log message ***

Revision 1.14  2005/06/28 16:38:16  carsten
*** empty log message ***

Revision 1.13  2005/05/03 18:46:46  carsten
*** empty log message ***

Revision 1.12  2005/04/11 13:12:35  carsten
*** empty log message ***

Revision 1.11  2005/04/07 11:41:08  carsten
*** empty log message ***

Revision 1.10  2005/03/18 19:47:17  carsten
*** empty log message ***

Revision 1.9  2005/03/15 16:08:41  carsten
*** empty log message ***

Revision 1.8  2005/03/10 16:04:38  carsten
*** empty log message ***

Revision 1.7  2005/03/08 16:04:45  carsten
*** empty log message ***

Revision 1.6  2005/02/16 19:36:38  carsten
*** empty log message ***

Revision 1.5  2005/02/09 15:11:43  carsten
*** empty log message ***

Revision 1.4  2005/01/07 15:23:05  carsten
*** empty log message ***

Revision 1.3  2005/01/06 19:46:00  carsten
*** empty log message ***

Revision 1.2  2005/01/06 19:00:32  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
