// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <dcfemlib.h>
using namespace DCFEMLib;

#include <longoptions.h>
#include <electrodeconfig.h>

#include <mesh2d.h>
#include <mesh3d.h>
#include <domain2d.h>
#include <domain3d.h>
#include <spline.h>
#include <trianglewrapper.h>
using namespace MyMesh;

using namespace std;

int main(int argc, char *argv[]) {

    LongOptionsList lOpt;
    lOpt.setLastArg("dat-file");
    lOpt.setDescription( (string) "Create a parametric or primary[-P] mesh from dat.file " );
    lOpt.insert( "help", no_argument, 'h', "This help" );
    lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
    lOpt.insert( "primary-mesh", no_argument, 'P', "Create primary mesh" );
    lOpt.insert( "Cylinder", no_argument, 'C', "Cylindrical domain [false]" );
    lOpt.insert( "smooth", no_argument, 'S', "Smooth 2D meshs [false]" );
    lOpt.insert( "Electrodenodes", no_argument, 'E', "Electrode positions are nodes [false]" );
    lOpt.insert( "boundarySpline", no_argument, 'B', "Use spline interpolation between Electrodepositions [unset = false]" );
    lOpt.insert( "equidistBoundary", no_argument, 'A', "Use equidistant dx-space between Electrodepositions [unset = false]" );
    lOpt.insert( "refractionLine", required_argument, 'r', "Optional file with structural boundary. Only 2d supported, simple x y list with lines separated by empty line" );
    lOpt.insert( "output", required_argument, 'o', "Output name [mesh]" );
    lOpt.insert( "boundary", required_argument, 'b', "Boundary in \% [10000]" );
    lOpt.insert( "dimension", required_argument, 'd', "Dimension [3]" );
    lOpt.insert( "paraBoundary", required_argument, 'p', "Parameter boundary in \% [5]" );
    lOpt.insert( "z-depth", required_argument, 'z', "Parameter depth in m [30 \% extension]" );
    lOpt.insert( "quality", required_argument, 'q', "quality [30.0 = very bad]" );
    lOpt.insert( "paraMaxArea", required_argument, 'a', "maximum tetrahedral solume for parameter domain [0.0]" );
    lOpt.insert( "dx", required_argument, 'x', "dx for local refinement [0.0]" );
    lOpt.insert( "para-local-dx", required_argument, 'l', "local refinement for para [0.21]" );

    if ( argc == 1 ) {
        lOpt.printHelp( argv[0] );
        return 1;
    }
    string profileFileName = argv[ argc - 1 ];
    string refraktorFileName = NOT_DEFINED;

    //** define default values
    bool help = false, verbose = false, primaryMesh = false, spline = false, equidistRefine = false;
    bool dipole = false, circle = false, Smooth = false, electrodeNodes = false, underWater = false;
    double quality = 0.0, boundary = 10000.0, boundaryPara = 5.0, paraDepth = 0.0;
    double paraArea = 0.0, dx = 0.0;
    int dimension = 3;
    double paraRefine = 0.21; // wenn ich 0.2 nehme gibt es seg. fault ?????
    string outfilename = "mesh";

    int option_char = 0, option_index = 0, tracedepth = 0;
    while ( ( option_char = getopt_long( argc, argv, "?hv"
                                         "P" // primary Mesh
                                         "D" // Dipole sources
                                         "C" // Circle geometry
                                         "S" // Smooth 2dmesh
                                         "E" // electrodeNodes
                                         "A" // equidist
                                         "B" // spline
                                         "U" // underwater
                                         "o:" // outfile
                                         "d:" // dimension
                                         "z:" // paradepth
                                         "b:" // boundary
                                         "p:" // Paraboundary
                                         "a:" // Paramaxarea
                                         "q:" // Quality
                                         "r:" // refraktor
                                         "l:" // lokal refinement for para
                                         "x:" // dx for lokal refinement
                                         , lOpt.long_options(), & option_index ) ) != EOF) {
        switch ( option_char ) {
        case '?':
            lOpt.printHelp( argv[0] );
            return 1;
            break;
        case 'h':
            lOpt.printHelp( argv[0] );
            return 1;
            break;
        case 'v':
            verbose = true;
            break;
        case 'P':
            primaryMesh = true;
            break;
        case 'S':
            Smooth = true;
            break;
        case 'D':
            dipole = true;
            break;
        case 'C':
            circle = true;
            dipole = true;
            break;
        case 'E':
            electrodeNodes = true;
            break;
        case 'A':
            equidistRefine = true;
            break;
        case 'B':
            spline = true;
            break;
        case 'U':
            underWater = true;
            break;
        case 'o':
            outfilename = optarg;
            break;
        case 'r':
            refraktorFileName = optarg;
            break;
        case 'd':
            dimension = atoi(optarg);
            break;
        case 'b':
            boundary = atof( optarg );
            break;
        case 'p':
            boundaryPara = atof( optarg );
            break;
        case 'a':
            paraArea = atof( optarg );
            break;
        case 'q':
            quality = atof( optarg );
            break;
        case 'x':
            dx = atof( optarg );
            break;
        case 'l':
            paraRefine = fabs( atof( optarg ) );
            break;
        case 'z':
            paraDepth = atof( optarg );
            break;
        default :
            cerr << WHERE_AM_I << " undefined option: " << (char)option_char << endl;
        }
    }

    if ( verbose ) {
        cout << "refine equidist = " << equidistRefine << endl;
        cout << "spline boundary = " << spline << endl;
        cout << "refine = " << paraRefine << endl;
        cout << "area = " << paraArea << endl;
        cout << "quality = " << quality << endl;
        cout << "smooth mesh = " << Smooth << endl;
        if ( underWater) {
            cout << "underwater = true" << endl;
        }
        if ( primaryMesh ) {
            cout << "primary dx = " << dx << endl;
        }
    }

    ElectrodeConfigVector profiles;
    profiles.load( profileFileName );
    vector < RealPos > electrodeList = profiles.electrodePositions();
    bool newFunct = true;

    if ( dimension == 2 && circle ) {
        Domain2D circleDomain;
        int marker = 0;
        if ( primaryMesh ) marker = 1;
        else marker = 2;
        circleDomain.createCircleMainDomain( electrodeList, paraRefine, equidistRefine, paraArea, marker, spline, -99 );

        if ( primaryMesh ) {
            circleDomain.refineElectrodes( dx );
        }

        if ( quality < 20.0 || quality > 35.0 ) quality = 30.0;

        circleDomain.save( outfilename + ".poly");
        Mesh2D circleMesh;
        circleDomain.createMesh( circleMesh, quality, false );

        if ( Smooth ) {
            //      circleMesh.improveMeshQuality( true, true, 1, 2);
            //      circleMesh.improveMeshQuality( true, true, 0, 2);
        }
        circleMesh.save( outfilename + MESHBINSUFFIX );
        if ( verbose ) circleMesh.showInfos();
    } else { // if no circle

        // check if x-y dimension and force
        if ( dimension == 2 ) {
            bool xDim = xVari( electrodeList ), yDim = yVari( electrodeList ), zDim = zVari( electrodeList );

            if ( verbose ) cout << "xdim: " << xDim << " yDim: " << yDim << " zDim: " << zDim << endl;
            if ( xDim && yDim && zDim ) {
                cerr << WHERE_AM_I << " possible 3d-data" << endl;
                exit(0);
            }

            vector < RealPos > tmpEList( electrodeList.size() );

            if ( xDim && yDim ) {
                for ( size_t i = 0; i < electrodeList.size(); i ++ ) {
                    tmpEList[ i ].setX( electrodeList[ i ].x() );
                    tmpEList[ i ].setY( electrodeList[ i ].y() );
                }
            } else if ( !xDim && yDim ) {
                for ( size_t i = 0; i < electrodeList.size(); i ++ ) {
                    tmpEList[ i ].setX( electrodeList[ i ].y() );
                    tmpEList[ i ].setY( electrodeList[ i ].z() );
                }
            } else if ( xDim && !yDim ) {
                for ( size_t i = 0; i < electrodeList.size(); i ++ ) {
                    tmpEList[ i ].setX( electrodeList[ i ].x() );
                    tmpEList[ i ].setY( electrodeList[ i ].z() );
                }
            }
            electrodeList = tmpEList;
            sort( electrodeList.begin(), electrodeList.end(), posX_less );
        }

        double xElmin = 0.0, xElmax = 0.0, yElmin = 0.0, yElmax = 0.0, zElmin = 0.0, zElmax = 0.0;

        findMinMaxVals( electrodeList, xElmin, xElmax, yElmin, yElmax, zElmin, zElmax );

        double maxSpan = max( xElmax - xElmin, yElmax - yElmin );
        double border = maxSpan * boundary / 100.0;
        double borderPara = maxSpan * boundaryPara / 100.0;

        if ( paraDepth  < 0.0 ) paraDepth = maxSpan * -paraDepth / 100.0;

        double xmin = xElmin - border, xmax = xElmax + border;
        double ymin = yElmin - border, ymax = yElmax + border;
        double zmax = 0, zmin = ( xmax - xmin ) * -0.7;

        //   cout << xElmin << " " << xElmax << " " << yElmin << " " << yElmax << endl;
        //   cout << xmin << " " << xmax << " " << ymin << " " << ymax << endl;
        // cout << maxSpan << " " << border << " " << borderPara << endl;

        Domain2D world2D;
        size_t nElecs = electrodeList.size();
        vector< Node * > boundaryNodes;

        if ( dimension == 2 ) {
            if ( newFunct ) {
                int mybc = HOMOGEN_NEUMANN_BC;
                if ( underWater ) mybc = 0;

                world2D.createInterface( electrodeList, paraRefine, equidistRefine, mybc, spline, -99 );
                Node * paraStart = &world2D.node( 0 );
                Node * paraEnd = &world2D.node( world2D.nodeCount() -1 );

                borderPara = 2.0 * electrodeList[ 0 ].distance( electrodeList[ 1 ] );

                Node * start = world2D.createNode( electrodeList[ 0 ] ); // left upper corner of para
                start->setX( start->x() - borderPara - border );
                boundaryNodes.push_back( start );
                Node * end = world2D.createNode( electrodeList.back() );
                end->setX( end->x() + borderPara + border );
                boundaryNodes.push_back( end ); // right upper corner
                Node * n1 = world2D.createNode( electrodeList[ 0 ] ); // left upper corner of paradomain
                n1->setX( n1->x() - borderPara );
                boundaryNodes.push_back( n1 );
                Node * n2 = world2D.createNode( electrodeList.back() ); // right upper corner of paradomain
                n2->setX( n2->x() + borderPara );

                double spanGlobal = end->pos().distance( start->pos() ); // left/right lower corner
                Node * endDown = world2D.createNode( end->pos() );
                endDown->setY( endDown->y() - spanGlobal * 0.7 );
                Node * startDown = world2D.createNode( start->pos() );
                startDown->setY( startDown->y() - spanGlobal * 0.7 );
                if ( underWater) {
                    start->setY( 0.0 );
                    end->setY( 0.0 );
                }

                world2D.createEdge( *paraEnd, *n2, HOMOGEN_NEUMANN_BC );
                if ( ! underWater) world2D.createEdge( *n2, *end, HOMOGEN_NEUMANN_BC );
                world2D.createEdge( *end, *endDown, MIXED_BC );
                world2D.createEdge( *endDown, *startDown, MIXED_BC );
                world2D.createEdge( *startDown, *start, MIXED_BC );
                if ( ! underWater ) world2D.createEdge( *start, *n1, HOMOGEN_NEUMANN_BC );
                world2D.createEdge( *n1, *paraStart, HOMOGEN_NEUMANN_BC );
                if ( underWater ) world2D.createEdge( *start, *end, HOMOGEN_NEUMANN_BC );

                double paraAreaTmp = paraArea;
                if ( !primaryMesh ) {
                    Node * n1Down = world2D.createNode( n1->pos() );
                    n1Down->setY( n1Down->y() - paraDepth );
                    Node * n2Down = world2D.createNode( n2->pos() );
                    n2Down->setY( n2Down->y() - paraDepth );

                    world2D.createEdge( *n1, *n1Down, 1 );
                    world2D.createEdge( *n1Down, *n2Down, 1 );
                    world2D.createEdge( *n2Down, *n2, 1 );
                    paraAreaTmp = 0;
                    world2D.createRegionMarker( RealPos( n1Down->pos() ) + RealPos( 0.1, 0.1 ), 2, paraArea);
                }
                world2D.createRegionMarker( RealPos( startDown->pos() ) + RealPos( 0.1, 0.1 ), 1, paraAreaTmp );


                //** bitte mit extremer Vorsicht geniessen.
                if ( profiles.areBuryElectrodesPresent() ) {
                    for ( int i = 0; i < world2D.nodeCount(); i ++ ) {
                        if ( world2D.node( i ).marker() == -99 ) world2D.node( i ).setMarker( HOMOGEN_NEUMANN_BC );
                    }
                    RVector depths( profiles.electrodeDepths() );

                    for ( uint i = 0; i < electrodeList.size(); i ++ ) {
                        world2D.createVIP( electrodeList[ i ] - RealPos( 0, depths[ i ] ), -99 );
                        if ( depths[ i ] > 0 ) {
                            world2D.createVIP( electrodeList[ i ] - RealPos( 0, depths[ i ] - paraRefine ), 0 );
                        }
                    }
                }

                if ( primaryMesh ) {
                    world2D.refineElectrodes( RealPos( 0.0, -dx * electrodeList[ 0 ].distance( electrodeList[ 1 ] ) ) );
                }

            } else { // if old function

                if ( paraRefine > ( 1.0 / 3.0 ) ) paraRefine = ( 1.0 / 3.0 );
                borderPara = 2.0 * electrodeList[ 0 ].distance( electrodeList[ 1 ] );

                RealPos prePos, thisPos, nextPos, midPos, minusPos, plusPos, normPos, newPos;
                Node * end = NULL, * start = NULL, * n1 = NULL, * n2 = NULL;

                if ( !circle ) {
                    start = world2D.createNode( electrodeList[ 0 ] ); // left uper corner
                    start->setX( start->x() - borderPara - border );
                    boundaryNodes.push_back( start );
                    n1 = world2D.createNode( electrodeList[ 0 ] ); // left uper corner of paradomain
                    n1->setX( n1->x() - borderPara );
                    boundaryNodes.push_back( n1 );
                    n2 = world2D.createNode( electrodeList.back() ); // right upper corner of paradomain
                    n2->setX( n2->x() + borderPara );
                }

                for ( size_t i = 0; i < nElecs; i ++ ) {
                    thisPos = electrodeList[ i ];

                    if ( i == nElecs-1 ) {
                        if ( !circle ) {
                            nextPos = n2->pos();
                            //	  nextPos = Line( thisPos, electrodeList[ i - 1 ] ).lineAt( -1.0 );
                        } else {
                            nextPos = electrodeList[ 0 ];
                            //	  cout << nextPos << endl;
                        }
                    } else nextPos = electrodeList[ i + 1 ];

                    if ( i == 0 ) {
                        if ( !circle ) {
                            prePos = n1->pos();
                            //	  prePos = Line( thisPos, electrodeList[ i + 1 ] ).lineAt( -1.0 );
                        } else {
                            prePos = electrodeList[ nElecs - 1 ];
                        }
                    } else prePos = electrodeList[ i - 1 ];

                    if ( paraRefine > 0.0 ) {
                        if ( i == 0 ) {
                            if ( !circle ) {
                                minusPos = Line( thisPos, prePos ).lineAt( paraRefine * 1.5 );
                                boundaryNodes.push_back( world2D.createNode( minusPos, world2D.nodeCount(), -1 ) );
                                minusPos = Line( thisPos, prePos ).lineAt( paraRefine / 2.0 );
                                boundaryNodes.push_back( world2D.createNode( minusPos, world2D.nodeCount(), -1 ) );
                            } else {
                                minusPos = Line( thisPos, prePos ).lineAt( paraRefine );
                                boundaryNodes.push_back( world2D.createNode( minusPos, world2D.nodeCount(), -1 ) );
                            }
                        } else {
                            minusPos = Line( thisPos, prePos ).lineAt( paraRefine );
                            boundaryNodes.push_back( world2D.createNode( minusPos, world2D.nodeCount(), -1 ) );
                        }
                    } else {
                        minusPos = Line( thisPos, prePos ).lineAt( 0.5 );
                    }

                    if ( primaryMesh || electrodeNodes || paraRefine == 0.0 ) {
                        boundaryNodes.push_back( world2D.createNode( thisPos, world2D.nodeCount(), -99 ) );
                    }

                    if ( paraRefine > 0.0 ) {
                        if ( i == ( nElecs - 1 ) ) {
                            if ( !circle ) {
                                plusPos = Line( thisPos, nextPos ).lineAt( paraRefine / 2.0 );
                                boundaryNodes.push_back( world2D.createNode( plusPos, world2D.nodeCount(), -1 ) );
                                plusPos = Line( thisPos, nextPos ).lineAt( paraRefine * 1.5 );
                                boundaryNodes.push_back( world2D.createNode( plusPos, world2D.nodeCount(), -1 ) );
                            } else {
                                plusPos = Line( thisPos, nextPos ).lineAt( paraRefine );
                                boundaryNodes.push_back( world2D.createNode( plusPos, world2D.nodeCount(), -1 ) );
                            }
                        } else {
                            plusPos = Line( thisPos, nextPos ).lineAt( paraRefine );
                            boundaryNodes.push_back( world2D.createNode( plusPos, world2D.nodeCount(), -1 ) );
                        }
                    } else {
                        plusPos = Line( thisPos, nextPos ).lineAt( 0.5 );
                    }

                    //** extra node in betwenn 2 Electrodes if paraRefine < 0.2;
                    if ( !circle && i != nElecs -1 && paraRefine <= 0.2 ) {
                        boundaryNodes.push_back( world2D.createNode( Line( thisPos, nextPos ).lineAt( 0.5 ), world2D.nodeCount(), -1 ) );
                    }

                    midPos = (minusPos + plusPos) / 2.0;
                    normPos = minusPos.norm( plusPos );

                    if ( !primaryMesh ) {
                        if ( electrodeNodes ) newPos = midPos + normPos * plusPos.distance( minusPos ) / 2.0;
                        else newPos = midPos + normPos * plusPos.distance( minusPos ) * sqrt(3.0)/2.0;
                    } else {
                        if ( dx == 0.0 ) dx = 0.1;
                        newPos = thisPos + normPos * dx;
                    }
                    //      if ( paraRefine > 0.0 || primaryMesh ){
                    if ( primaryMesh ) {
                        world2D.createNode( newPos, world2D.nodeCount(), 1 );
                    }

                    //  	cout << plusPos.distance( newPos) << " "<< minusPos.distance( newPos) << " " << plusPos.distance( minusPos) << endl;
                    //  	cout << plusPos.angle( newPos, minusPos, DEG ) << " " << minusPos.angle( newPos, plusPos, DEG ) << " "
                    //  	     << newPos.angle( plusPos, minusPos, DEG ) << endl;
                }

                if ( !circle ) {
                    boundaryNodes.push_back( n2 );
                    end = world2D.createNode( electrodeList.back() );
                    end->setX( end->x() + borderPara + border );
                    boundaryNodes.push_back( end ); // right upper corner
                }

                for ( size_t i = 0; i < boundaryNodes.size() - 1; i++ ) { // for all surface nodes
                    world2D.createEdge( *boundaryNodes[ i ], *boundaryNodes[ i + 1 ], -1 );
                }

                if ( !circle ) {
                    double spanGlobal = end->pos().distance( start->pos() );
                    double spanLokal = n2->pos().distance( n1->pos() );
                    Node * endDown = world2D.createNode( end->pos() );
                    endDown->setY( endDown->y() - spanGlobal * 0.7 );
                    Node * startDown = world2D.createNode( start->pos() );
                    startDown->setY( startDown->y() - spanGlobal * 0.7 );

                    world2D.createEdge( *start, *startDown, -2 );
                    world2D.createEdge( *startDown, *endDown, -2 );
                    world2D.createEdge( *endDown, *end, -2 );
                    double paraAreaTmp = paraArea;
                    if ( !primaryMesh ) paraAreaTmp = 0;
                    world2D.createRegionMarker( RealPos( startDown->pos() ) + RealPos( 1.0, 1.0 ), 1, paraAreaTmp );

                    if ( !primaryMesh ) {
                        Node * n1Down = world2D.createNode( n1->pos() );
                        n1Down->setY( n1Down->y() - paraDepth );
                        Node * n2Down = world2D.createNode( n2->pos() );
                        n2Down->setY( n2Down->y() - paraDepth );

                        world2D.createEdge( *n1, *n1Down, 1);
                        world2D.createEdge( *n1Down, *n2Down, 1);
                        world2D.createEdge( *n2Down, *n2, 1);
                        world2D.createRegionMarker( RealPos( n1Down->pos() ) + RealPos( 1.0, 1.0 ), 2, paraArea);
                    }
                } else { //** if circle
                    world2D.createEdge( *boundaryNodes.back(), *boundaryNodes[ 0 ], -1 );
                    world2D.createRegionMarker( averagePosition( electrodeList ), 2, paraArea );

                    //** allways create an reference node
                    world2D.createNode( averagePosition( electrodeList ), world2D.nodeCount(), -999 );
                    if ( primaryMesh ) {
                        world2D.createNode( averagePosition( electrodeList ) + RealPos( dx, 0.0, 0.0 ), world2D.nodeCount(), 0 );
                    }
                }
            } // end oldFunction
            //** Finish build world

            if ( refraktorFileName != NOT_DEFINED ) {
                fstream file;
                openInFile( refraktorFileName, &file );
                vector < string > row;

                vector < vector < RealPos > > refraktorVec;
                vector < RealPos > refraktor;
                std::vector < unsigned int > refraktorID;
                refraktorVec.push_back( refraktor );
                refraktorID.push_back( 1 );

                while ( !file.eof() ) {
                    row = getRowSubstrings( file );
                    switch ( row.size() ) {
                    case 0:
                        refraktorVec.push_back( refraktor );
                        refraktorID.push_back( refraktorVec.size()-1 );
                        break;
                    case 2:
                        refraktorVec.back().push_back( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ), 0.0 ) );
                        break;
                    case 3:
                        refraktorVec.back().push_back( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ), 0.0 ) );
                        refraktorID[ refraktorVec.size() -1 ] = toInt( row[ 2 ] );
                        break;
                    default:
                        std::cerr << "cannot interprete dataformat for refraktorFileName: "<< refraktorFileName << " " << row.size() << std::endl;
                    }
                }
                file.close();

                for ( uint j = 0; j < refraktorVec.size(); j ++ ) {
                    if ( refraktorVec[ j ].size() > 0 ) {
                        int oldNodeCount = world2D.nodeCount();
                        vector < Node * > tmpref;
                        for ( uint i = 0; i < refraktorVec[ j ].size(); i ++ ) {
                            tmpref.push_back( world2D.createVIP( refraktorVec[ j ][ i ] ) );
                        }
                        for ( uint i = 0; i < tmpref.size()-1; i ++ ) {
                            world2D.createEdge( *tmpref[ i ], *tmpref[ i + 1 ], refraktorID[ j ] );
                        }
                    }
                }
            }

            world2D.save( outfilename + ".poly");
            if ( quality < 20.0 || quality > 35.0 ) quality = 30.0;
            Mesh2D outmesh( world2D.createMesh( quality, verbose ) ) ;

            if ( Smooth ) {
                outmesh.improveMeshQuality( true, true, 1, 2 );
            }
            outmesh.save( outfilename + MESHBINSUFFIX );
            if ( verbose ) outmesh.showInfos();
        } else if ( dimension == 3 ) {

            Domain3D world;
            Domain3D para;
            if ( !circle ) {

                world.createSimpleMainDomain( RealPos( xmin, ymin, zmax),RealPos( xmax, ymax, zmin) );
                world.insertRegionMarker( RealPos( xmin + 1, ymin + 1, zmin + 1), 1, 0 );

                //       para = createCube( RealPos( 0.0, 0.0, 0.0 ), 2, paraArea, false );
                //       para.save( "testCube" );
                createCuboid( para, 2, 1, paraArea, false );
                para.translate( 0, 0, -0.5 );

                if ( verbose ) cout << "ParaDepth = " << paraDepth << endl;

                para.scale( ( xElmax - xElmin ) + 2.0 * borderPara,
                            ( yElmax - yElmin ) + 2.0 * borderPara, paraDepth );


                double xminPara = ( xElmax - xElmin ) / -2.0 - borderPara;
                double yminPara = ( yElmax - yElmin ) / -2.0 - borderPara;

                //    cout << xminPara << " " << yminPara << endl;
                para.translate( xElmin - xminPara - borderPara,
                                yElmin - yminPara - borderPara, 0.0 );

                world.link( para );

                Node *n;
                for ( size_t i = 0; i < nElecs; i ++ ) {
                    n = world.createVIP( electrodeList[ i ], -99 );
                    // n->setZ( 0.0 );
                }

                double dx = 0.0;
                if ( paraRefine == 0.0 ) {
                    dx = electrodeList[ 0 ].distance( electrodeList[ 1 ] ) / 2.0;
                } else {
                    dx = paraRefine;
                }
                for ( size_t i = 0, imax = world.nodeCount(); i < imax; i ++ ) {
                    if ( world.node( i ).marker() == -99 ) {
                        world.createNode( world.node( i ).pos() - RealPos( 0.0, 0.0, dx), world.nodeCount(), 0 );
                    }
                }

                //   double x = 0.0;
                //     for ( int i = 0; i < 8; i ++ ){
                //       x = 1.25 + i * 2.5;
                //       world.createLineOfVIPs( RealPos( x, 1.25, -1.0), RealPos(x, 32.0 - 1.25, -1.0), 16, 0 );
                //     }
            } else { // if cylinder and dim == 3
            }
            world.saveTetgenPolyFile( outfilename + ".poly" );
        }
    } // if dim3
    return 0;
}

/*
>>>>>>> 1.35
$Log: createParaMesh.cpp,v $
Revision 1.37  2009/07/09 22:31:04  carsten
some small fixes and simple 1.check-script for examples

Revision 1.36  2009/04/15 17:08:07  carsten
*** empty log message ***

Revision 1.35  2009/04/15 17:06:47  thomas
no message

Revision 1.34  2009/04/09 09:45:31  thomas
minor changed in scripts, readme and tutorial

Revision 1.33  2008/11/21 07:51:57  thomas
only changes in documentation

Revision 1.32  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.31  2007/06/20 18:57:17  carsten
*** empty log message ***

Revision 1.30  2007/01/29 18:28:27  thomas
*** empty log message ***

Revision 1.29  2006/11/01 18:46:08  carsten
*** empty log message ***

Revision 1.28  2006/08/27 18:58:03  carsten
*** empty log message ***

Revision 1.27  2006/08/21 15:21:11  carsten
*** empty log message ***

Revision 1.26  2006/07/20 16:05:47  carsten
*** empty log message ***

Revision 1.25  2006/06/29 12:09:52  thomas
underwater option added

Revision 1.24  2006/03/13 18:33:49  carsten
*** empty log message ***

Revision 1.22  2006/03/02 23:45:31  carsten
*** empty log message ***

Revision 1.21  2006/03/02 18:30:20  carsten
*** empty log message ***

Revision 1.20  2006/03/01 17:25:14  carsten
*** empty log message ***

Revision 1.19  2005/12/20 13:41:40  carsten
*** empty log message ***

Revision 1.18  2005/11/04 20:21:36  carsten
*** empty log message ***

Revision 1.17  2005/10/30 18:53:04  carsten
*** empty log message ***

Revision 1.16  2005/10/24 09:52:08  carsten
*** empty log message ***

Revision 1.15  2005/10/20 12:08:34  carsten
*** empty log message ***

Revision 1.14  2005/10/18 12:48:11  carsten
*** empty log message ***

Revision 1.13  2005/10/17 15:10:17  carsten
*** empty log message ***

Revision 1.12  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.11  2005/10/06 17:38:36  carsten
*** empty log message ***

Revision 1.10  2005/09/12 18:50:50  carsten
*** empty log message ***

Revision 1.9  2005/09/12 10:38:36  carsten
*** empty log message ***

Revision 1.8  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.7  2005/07/13 17:05:21  carsten
*** empty log message ***

Revision 1.4  2005/06/23 20:15:32  carsten
*** empty log message ***

Revision 1.3  2005/06/01 18:26:51  carsten
*** empty log message ***

Revision 1.2  2005/05/03 15:55:34  carsten
*** empty log message ***

Revision 1.1  2005/03/29 16:57:46  carsten
add inversion tools

*/
