// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <ctime>
#include <longoptions.h>

#include <myvec/vector.h>
using namespace MyVec;

using namespace std;

int main( int argc, char *argv[] ){

  setbuf(stdout, NULL);

  LongOptionsList lOpt;
  lOpt.setLastArg( "vector" );
  lOpt.setDescription( "Convert a vector file from ascii to binary. The ascii format contains a simple column of vector values. See also vb2a." );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "help", no_argument, 'h', "This help" );

  if ( argc < 2 ) lOpt.printHelp( argv[0] );

  bool help = false, verbose = false;
  int option_char = 0, option_index = 0;
  while ( ( option_char = getopt_long( argc, argv,
				       "?"
				       "h"
				       "v",
				       lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'v': verbose = true; break;
    default : cerr << WHERE_AM_I << " undefined option: " << option_char << endl;
    }
  }

  RVector v;

  for ( int i = 1; i < argc; i ++ ){
    if ( v.load( argv[ i ], Ascii ) ) v.save( argv[ i ], Binary );
    if ( verbose ) cout << ".";
  }
  if ( verbose ) cout << endl;

  return 0;
}

/*
$Log: va2b.cpp,v $
Revision 1.5  2010/06/22 14:42:47  thomas
*** empty log message ***

Revision 1.4  2010/06/18 10:17:59  carsten
win32 compatibility commit

Revision 1.3  2008/11/21 07:51:57  thomas
only changes in documentation

Revision 1.2  2005/06/01 18:26:51  carsten
*** empty log message ***

Revision 1.1  2005/02/11 14:37:42  carsten
add vb2a and va2b in path apps/vector

*/
