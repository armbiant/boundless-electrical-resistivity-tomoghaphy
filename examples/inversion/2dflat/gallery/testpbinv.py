import pygimli as pg
from pygimli.physics import ert
import pygimli.meshtools as mt
from matplotlib import colors, ticker


data = ert.load('gallery.dat')
data["err"] = ert.estimateError(data, relativeError=0.02)
if 0:
    mgr = ert.ERTManager(data)
    mgr.invert()
    # %%
    ax, cb = mgr.showResult()
    # cb.ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
    # cb.ax.ticklabel_format(style="plain")
    # cb.formatter.set_powerlimits((0, 8))
    # %%
    dsfsf

mesh = mt.createParaMesh(data.sensorPositions(), paraDX=0.5, paraDepth=10,
                         paraBoundary=10, boundary=4.4, quality=34.5)
pg.show(mesh, markers=True)
f = ert.ERTModelling(sr=False)
f.setMesh(mesh)
f.data = data
f.setRegionProperties(1, background=True)

# f.createRefinedForwardMesh(True)
tLog = pg.trans.TransLog()

inv = pg.Inversion(fop=f, verbose=True)
# inv.setData()
inv.transData = tLog
inv.transModel = tLog
res = inv.run(data["rhoa"], data["err"], lam=20)
pg.show(f.paraDomain, res, colorBar=True, cMap="Spectral_r")
pg.wait()
