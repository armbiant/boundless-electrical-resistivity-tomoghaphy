#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    This program is part of pybert
    Visit http://www.resistivity.net for further information or the latest version.
"""

import sys, traceback
import getopt
from os import system

try:
    import pygimli as g
except ImportError:
    sys.stderr.write('''ERROR: cannot import the library 'pygimli'. Ensure that pygimli is in your PYTHONPATH ''')
    sys.exit( 1 )

from pybert.importer import *

def usage( exitcode = 0, comment = '' ):
    print(comment)
    print(__doc__)
    exit( exitcode )

def main( argv ):
    
    # OptionParser is deprecated since python 2.7, use argparse 
    from optparse import OptionParser

    parser = OptionParser( "usage: %prog [options] data-file"
                            , version="%prog: " + g.versionStr() 
                            , description = "Edit data file with gimli unified file format" )
    parser.add_option("-v", "--verbose", dest="verbose", action = "store_true", default = False
                            , help="Be verbose." )
    parser.add_option("-o", "--output", dest = "outFileName", metavar = "File", default = None
                            , help = "Filename for the resulting data file." )
    parser.add_option("", "--invert", dest = "invert", action="store_true", default = False
                            , help = "Invert current and potential electrodes." )
                            
    (options, args) = parser.parse_args()

    importFileName = None

    if len( args ) == 0:
        parser.print_help()
        print("Please add a data file.")
        sys.exit( 2 )
    else:
        importFileName = args[ 0 ];

    if options.outFileName is None:
        options.outFileName = importFileName[0:importFileName.find('.shm')] + '.edt'
        
    if options.verbose:
        print((options, args))
        print(("verbose =", options.verbose))
        print(("project =", importFileName))
        print(("output =", options.outFileName))
            
    data = importData( importFileName, format = 'auto', verbose = options.verbose )
    
    if options.invert:
        a = g.RVector( data('a') )
        b = g.RVector( data('b') )
        data.set('a', data('m') )
        data.set('b', data('n') )
        data.set('m', a )
        data.set('n', b )
    
    
    # editiere elektrodenpositionen und skalierung
    # filtere nach allem (token, elektrodenid)
    # optimiere (umsortieren) nach Multikanalfähigkeit (zbsp. für syscall pro ) + optionales auffüllen ungenutzter kanäle??
    
    # cross check to protect original file
    if options.outFileName != importFileName:
        data.save( options.outFileName )
    else:
        raise Exception( "Warning! will not overwrite input data: " + str(importFileName) + " desired outname: " + str( options.outFileName ) )
    
if __name__ == "__main__":
    main( sys.argv[ 1: ] )